(function(angular) {

   
 var env = {};


if(window){  
  Object.assign(env, window.__env);
}

	var app =  angular.module('shadownav', ['ui.bootstrap','checklist-model','socket.io','angular-web-notification']);
	app.constant('__env', env);
	app.config(function ($socketProvider) {
	 			console.log("IN CONFIG --------------socketProvider----------X");
	            $socketProvider.setConnectionUrl(__env.clientWSUrl);
	            $socketProvider.setReconnect(true);
				$socketProvider.setReconnectionDelay(1000);
				$socketProvider.setMaxReconnectionAttempts(100);
	                
	        });



   app.controller('shadownavCtrl', function shadownavCtrl( $uibModalStack,$timeout,$q,$uibModal,$scope,$rootScope, $interval, $http, $window,$socket){
   console.log("API URL ------->"+__env.clientWSUrl) 

	 $scope.viewStocks = function() {             
                console.log("stocksEdit--> " + $window.location.host )
                //$state.go('stocksEdit');               
                 $window.location.href = 'stocksEdit.html';
        }
		 
		$socket.on('connect', () => {
		       console.log('Successfully connected to socket!');
		});
		console.log($socket);
		
		$socket.on('disconnect', () => {
		       console.log('Socket disconnected!');
		       emailjs.init("user_rRhmrTy6ydBNGpItVhiH3");
	    	emailjs.send("mailjet","template_websocket",{email: err})
	                 .then(function(response) {
	                   console.log("SUCCESS. status=%d, text=%s", response.status, response.text);
	                 
	                 })
		});

		$socket.on('connect_error', () => {
		       console.log('Socket connect_error!');
		       emailjs.init("user_rRhmrTy6ydBNGpItVhiH3");
	    	emailjs.send("mailjet","template_websocket",{email: err})
	                 .then(function(response) {
	                   console.log("SUCCESS. status=%d, text=%s", response.status, response.text);
	                 
	                 })
		});

		$socket.on('error', () => {
		       console.log('Socket error!');
		       emailjs.init("user_rRhmrTy6ydBNGpItVhiH3");
	    	emailjs.send("mailjet","template_websocket",{email: err})
	                 .then(function(response) {
	                   console.log("SUCCESS. status=%d, text=%s", response.status, response.text);
	                 
	                 })
		});
		//Declaration of global variables
		$rootScope.socket = $socket;
	    $rootScope.auth = null
	    $rootScope.token ;
		var update_in_progress = false,
		status = 'LIvE',
		api_server = status == 'test' ? 'api2.localhost' : 'api2.stockpulse.de',
		dev_call = status == 'test' ? 'true' : 'false';
		$scope.IsloggedIn = false;
		$scope.summaryNav ;
		var plDailyUpdatedInDb = false;
		var numberOfRetry = 0;
	    var exchangeRate;		  
		var exchangeRatePrev;
		//Prev exchangerate has to be checked when there is a refresh of the dashboard too
		var exchangePrevRateExists = false;
		var exchangeClosedRateExists = false;
		var yesterdayClosePriceExists = false;
		var oderHistoryTableChanged = false;
		var prevPLChanged = false;
		var ordersPlaced = [];
		var ordersForConfirmation = [];
		var closedPriceData = [];
		var dailyPLData = [];
		var dailyPLDataInDB = [];
	    $scope.orderList  =[]
		$scope.StartClosedPriceData = [];
		var totalNAV ;
		var noDataFound = false;
		var DataIn = false;
		$scope.AssignedDate = Date;
		$scope.strList =[];
		$scope.orderIDListRejected =[];
		$scope.message ;
		$scope.selectedItems = [];
		var currrentPriceFromTickData = [];
		var username,password;
        var holidayList = [];  
		$scope.stocksList= []	
		//Example Json reading        
		$http.get('data.json')
			.then(function success(data) {
					console.log("strategy_config");
					console.log(data);	
					$scope.stocksList = data.data					
					console.log($scope.stocksList);	
					//$scope.stocksList = data.data		
					
			    })

		
	$scope.refreshAutoConfirm = function() {
			      $http({
	                    url: __env.apiUrl+':'+__env.apiUrlPort+'/strategyOnOffs',
	                    method: "GET",                                         
	                    headers: {
	                         'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
	                    }
	                          
	                })
	                .then(function (data){
	                      console.log("refreshAutoConfirm -->	");
	                      console.log(data.data);
	                      $scope.strList  = data.data;

	                },function (error){
	                	console.log(error);

	                 });

	            }    

			 $scope.check = function(value, checked) {
	 		
						    var idx = $scope.strList.indexOf(value);
						    console.log("idx...."+idx);
						    if (idx >= 0 ) {
						    	$scope.selectedItems.push(value);
						       console.log("!checked...."+value );
						    }
						  
			 };



	  /**
		* @name getTitleIDsAndSubscribeTick
		* @desc 
		* @param  
		* @returns returns current price from tick data 
	    **/
		function getTitleIDsAndSubscribeTick($scope, $http) {
			
			
			
				$.ajax(__env.clientApiUrl+'/shadow_nav/?dev_call='+dev_call, {
				    error: function(err) { 
					console.log(err);
				
				},
				success: function(data) {
				
					console.log("POSITION data from the API query  responseText ->");				
					var listOfTitleIDs = [];
					$scope.reportItems = data;		
					$scope.strategyData= $scope.reportItems.strategies;
					for (var strategy in $scope.strategyData) {
						//console.log($scope.strategyData[strategy].positions);
						angular.forEach($scope.strategyData[strategy].positions, function(obj, key) {
						//console.log(key + ': ' + obj);
						//Get the list of title_ids from the position data to query for the current price in tick data									
				    	angular.forEach(obj, function(positionsList, key) {
			  	    	    if(key == "title_id")
					    		listOfTitleIDs.push(positionsList)
					    	})
				    	
				    	})
				    	// This is to calucate the totalNAV for the portfolio.
				    	//to get the current price of DAX CFD basis position invested. 
				    	listOfTitleIDs.push("4000003")
				    	console.log(listOfTitleIDs);
					}

				 $rootScope.socket.emit('authorize', { username: "radhika.ramamurthy@catanacapital.de", password: "f7aQw3419x"}, function(err,data) {
			    // register for streams after auth has completed
			    console.log(data); 
			    //Subscribe to title ticks
				$rootScope.socket.emit('subscribe title ticks',  listOfTitleIDs, function(err,data) {
				//	console.log(data);
				$rootScope.socket.on('tick', function(data) {
					//	   console.log(data);
					// console.log("in TICKKKKKKKK")
				    var currClosePrice = 0; 
				    for(var id in listOfTitleIDs){
				        if(listOfTitleIDs[id] == data.id){
                			//console.log("in FIRST "+listOfTitleIDs[id] );
                            if(data.l == null && data.a != null ){
                                //       console.log("in SEC "+data.a );
                                updateTickData(currrentPriceFromTickData,data.id,data.a,data.t)
                            }else if(data.l == null && data.a == null ){
                                 //      console.log("in THIRD "+data.b );
                                updateTickData(currrentPriceFromTickData,data.id,data.b,data.t)
                            }else{
                                 //       console.log("in Fourth "+data.l);
                                updateTickData(currrentPriceFromTickData,data.id,data.l,data.t)
                            }
                        }
							
			        }        
				})   		    
				if (err && Config.debugMode) console.error(err);
				 });
			})
			},headers: {
		  		"Authorization": "Basic " + $rootScope.auth
				  	}
			});		
		
	}

	/**
		* @name submitAutoConfirm
		* @desc 
		* @param  
		* @returns returns current price from tick data 
	    **/
	$scope.submitAutoConfirm = function() {


	       	var dataObj =$scope.selectedItems;   		
			var autoConfirmFlagInDB ;
			var autoConfirmFlagInDBError ;
	   		for (var items in $scope.selectedItems) {   
	   		  console.log("Data selectedItems-----------------------> " +$scope.selectedItems[items]._id);				
	   		  $http({
	                url: __env.apiUrl+':'+__env.apiUrlPort+'/strategyOnOffUpdate?_id='+$scope.selectedItems[items]._id+'&checked='+$scope.selectedItems[items].checked+'&username='+username+'&password='+password,
	                method: "GET",                                         
	                headers: {
	                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
	                }
	                          
	            })
	            .then(function (data){
	              	dailyPLData = data;
					console.log(dailyPLData);
					autoConfirmFlagInDB = true;		
					//$scope.message="Data posted successfully";	
                },function (error){
                	console.log("error-----------------");
                	autoConfirmFlagInDBError = true;
                	//exit;
                });
			}
			$window.setTimeout(updateMessage, 1000);
			function updateMessage(){

			       	if(autoConfirmFlagInDB)	{
			       		$scope.selectedItems.length = 0;
			       		alert('Configuration data saved successfully.');
			       	}
			        else if(autoConfirmFlagInDBError)
			        	alert('Problem saving configuration data!');
			}	
  	};

		/**
		* @name submitForm
		* @desc 
		* @param  
		* @returns returns current price from tick data 
	    **/
		$scope.submitForm = function() {

	        var dayName = getDayOfTheWeek();
	     			var startDate;
					var endDate;
					if(dayName === "Monday"){
					  
						startDate = new Date();
			          	startDate.setDate(startDate.getDate() - 3); 
				 		endDate = new Date();
						endDate.setDate(endDate.getDate() - 3);
					}else{
						startDate = new Date();	
						startDate.setDate(startDate.getDate() - 1);
						endDate = new Date();
						endDate.setDate(endDate.getDate() - 1);				      	  
					}
		           		// Set hours
		                startDate.setHours(08);
		                endDate.setHours(21);

		                // Then set minutes
		                startDate.setMinutes(00);
		                endDate.setMinutes(30);

		                // Then set seconds
		                startDate.setSeconds(00);
		                endDate.setSeconds(00);
					$.ajax(__env.apiUrl+':'+__env.apiUrlPort+'/dailyplsPrevUpdate?startDate='+startDate.toISOString()+'&endDate='+endDate.toISOString()+'&officialPL='+$scope.officialPL, {
				  		error: function(err) {
						console.log(err);
						update_in_progress = false;
					},
					success: function(data) {   
						dailyPLData = data;
						console.log(dailyPLData);
					},
					headers: {
					    'Content-Type': 'application/x-www-form-urlencoded'
					},
					}); 
      
		};


		/**
		* @name submitForm
		* @desc 
		* @param  
		* @returns returns current price from tick data 
	    **/
		//Authenticate the user first and then only display the Dashboard.
		$scope.authUser =  function(socket) {
		
		  username = $("#navbar form input[name='username']").val(),
		  password = $("#navbar form input[name='password']").val();
		  console.log("username - " + ""+username+"")
		  if(""+username+"" != "bastian.lechner@catanacapital.de" && ""+username+"" != "holger.knauer@catanacapital.de" && ""+username+"" != "radhika.ramamurthy@catanacapital.de")
		  {
		 	alert('Please enter MANGANED ACCOUNT username AND password.');
		  }else{
			if (!username || !password) {
				alert('Please enter both username AND password.');
			} else {
			$rootScope.auth = btoa(username+':'+password);
			//console.log($rootScope.auth);
			$.ajax(__env.clientApiUrl+'/titles/3212', {
	    		error: function(err) {
	    		console.log(err);
	    		if (err.responseJSON && err.responseJSON.en)
	    			alert(err.responseJSON.en+username+' '+password);
	    		else
	    			alert('Login not successful. '+username+' '+password);
	    		},
	    		success: function(data) {
					getDailyPLForTotalNav(function(data) { 
						dailyPrevDayPLDataInDB = data;
						for (var i = 0; i < dailyPrevDayPLDataInDB.length; i++) {
						    //Check if todays PL is already in DB, current days PL is saved at 17:30 everyday, 
						    //If the weekly PL is calculated after that then consider that PL from DB, else running PL
						    totalNAV = dailyPrevDayPLDataInDB[i].totalnav
						   console.log(dailyPrevDayPLDataInDB[i].totalnav)
						}    
 					})
	    				getHolidayList(holidayList);  	    				  
					    //IF there is an unconfirmed order in order_history on restart of the application it should show up
						//First get the Strategyname and search in the Query from the Order history API call. 			    				
						getOrderHistoryForOderConfirmation();
						console.log("WEBSOCKET CONNECTED----> "+$rootScope.socket.connected);
						//console.log(ordersForConfirmation);						
						function onOrderHistoryReady(){
						//	console.log("GAME ON DataIn"+DataIn);
							//TO deal with async nature of API wait till we get a true from the result of the API call
									 if(DataIn){
										for (var orders in ordersForConfirmation)    {	
											if(ordersForConfirmation[orders].confirmed_at == null && ordersForConfirmation[orders].rejected_at == null)  {
													//positionNotInOrder= false;
													$scope.orderList.push((ordersForConfirmation[orders]));
													if($scope.orderList.indexOf(ordersForConfirmation[orders].order_id) !== -1) {
														console.log("ORDER ID ALREADY EXISTS");

														}													
													}											
										}
										if($scope.orderList.length > 0)  {
											var size,type,name,tradeType;
											for (var olist in $scope.orderList) {
										    	//console.log( $scope.orderList[olist].order_id); 
										        size = $scope.orderList[olist].size 
										        type = $scope.orderList[olist].type
										        name =  $scope.orderList[olist].name 

										    } 
										    if(size > 0 )
											 	tradeType="BUY"
											else
											 	tradeType="SELL";
					  						var signalBody = name +" - "+ tradeType;
										 	$scope.openModal();  
					                        webNotification.showNotification("NEW ORDER", {
					                        	 tag: 1,
								                body:signalBody,
								             	requireInteraction: true,
								                icon:'http://www.catanacapital.de/images/logo.png',
								                onClick: function onNotificationClicked() {
													        if (hideNotification) {
													          hideNotification();
													        }
													    }		
								               //auto close the notification after 4 seconds (you can manually close it via hide function)
								            }, function onShow(error, hide) {
								                if (error) {
								                        window.alert('Unable to show notification: ' + error.message);
								                } else {
								                    hideNotification = hide;
								                         
								                }
								            });
					                   }     
									}									
						}
			$window.setTimeout(onOrderHistoryReady, 1000);

			function compareOrderListIDs(order_id,orderList){
			var sameOrderID = false;
	 			for (var orderId in orderList)    {	
				//	console.log('compareOrderListIDs FROM WEBSOCKET' + orderList[orderId].order_id);
					if(orderList[orderId].order_id == order_id )
						sameOrderID=  true;
				}
				return sameOrderID;
			}

			function preOrderProcess(){
			 //Now campare with the order to be confirmed in the ORDER_HISTORY table. 
			 //This is just to double check the orders			     
				for (var orders in ordersForConfirmation)    {	
					 //console.log("IN orders in getOrderHistoryForOderConfirmation>> " +ordersForConfirmation[orders].confirmed_at );
					if(ordersForConfirmation[orders].confirmed_at == null && ordersForConfirmation[orders].rejected_at == null){
						if(!compareOrderListIDs(ordersForConfirmation[orders].order_id,$scope.orderList))  {
						//	 	console.log('TIME PASS Shown.');
							$scope.orderList.push((ordersForConfirmation[orders]));
						}
					}
				}
	            console.log($scope.orderList);
	            if($scope.orderList.length > 0)  {	
	            	$scope.openModal();  
			        webNotification.showNotification("NEW ORDER", {
                 	 	tag: 1,
	                	body:signalBody,
			            requireInteraction: true,
						icon:'http://www.catanacapital.de/images/logo.png'
						//auto close the notification after 4 seconds (you can manually close it via hide function)
					}, function onShow(error, hide) {
					    if (error) {
					        window.alert('Unable to show notification: ' + error.message);
					    } else {
					        console.log('Notification Shown.');
					    }
					});
			    } 
			}
	        
	        $rootScope.socket.on('new order', function(data) {
		        $scope.message=null
		        console.log('new order');
		        console.log( JSON.stringify(data));
		        console.log( data.name);
		        if(data.name != "Catana Virtual MDAX Intensity") 
		          	 checkAutoConfirm((data.strategy_id).toString(),data);	           	                
	            });

	        function autoConfirmResult(data,flag){
	            	console.log("flag " + flag)	;	
		          	if(data.name != "Catana Virtual MDAX Intensity")   { 					    	
					   	if(!flag){
					        $scope.orderList.push((data))
					        if($scope.orderList.length > 0)  {	
					           var size,type,name,tradeType;
								for (var olist in $scope.orderList) {
								    //	console.log( $scope.orderList[olist].order_id); 
							        size = $scope.orderList[olist].size 
							        type = $scope.orderList[olist].type
							        name =  $scope.orderList[olist].name 
							    } 
								if(size > 0 )
								 	tradeType="BUY"
								else
									tradeType="SELL";
		  						var signalBody = name +" - "+ tradeType;
				            	$scope.openModal();  
						        webNotification.showNotification("NEW ORDER", {
			                 	 	tag: 1,
				                    body:signalBody,
				                    requireInteraction: true,
				                    icon:'http://www.catanacapital.de/images/logo.png',	
									onClick: function onNotificationClicked() {
								        if (hideNotification) {
								          hideNotification();
								        }
								    }				                    
				                    //auto close the notification after 4 seconds (you can manually close it via hide function)
							    }, function onShow(error, hide) {
					                if (error) {
					                    window.alert('Unable to show notification: ' + error.message);
					                } else {
									    console.log('Notification Shown.');
									    hideNotification = hide;
								    }
								});
							} 
					    } 
	 				} 
			} 

            function checkAutoConfirm(id,orderData){
	         	console.log("IN checkAutoConfirm " + id);
	        	var flag = false;
	          	$.ajax(__env.apiUrl+':'+__env.apiUrlPort+'/strategyOnOffs', {
					error: function(err) {
						console.log(err);
					},
					success: function(data) {   
						console.log("checkAutoConfirm	");
	                    console.log(data);
	                    $scope.strList  = data;
	                    for (var list in $scope.strList) {
	                     	console.log( $scope.strList[list]); 
						   	if(id === $scope.strList[list].strategy_id &&  $scope.strList[list].checked) {
						   		console.log("IJNc checkAutoConfirm " +$scope.strList[list].checked)
						   		flag =  true;
	 						} 
						} 		    
					},
					headers: {
					    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
					},
				}).done(function(result) 
				    {
				       autoConfirmResult(orderData,flag)
				}); 			
			}	       
			
			$rootScope.socket.on('complete order', function(data) {
	        	console.log('complete order');
	            console.log((data));          
				if(data.name != "Catana Virtual MDAX Intensity")   {   
		            $scope.message = ("Data Posted successfully" );
			  	    $scope.orderList.length = 0;
					$scope.IsloggedInPopUp = false;
					 // console.log("EMPTY orderLsit" + $scope.orderList);
					getOrderHistory();
					$timeout(function() {
					// $uibModalInstance.close($scope.selected.orderList);
						$uibModalStack.dismissAll();
					}, 5000);	 
				}  		           
      		});
	    	$scope.IsloggedIn = true;
	    	getTitleIDsAndSubscribeTick($scope, $http)
	    	updateDashboard($scope, $http);
	    	$interval(function(){
		    	if(numberOfRetry < 3){
		     		console.log("NUMBER OF RETRY WITHIN LIMITS--------------->  ");
		 			updateDashboard($scope, $http);
		 	  	}
		 	},5000);    				
	    	//Get the present Exchange rate so that this will be called everyminute : TO-DO Change this to Dollar instead of Pound
	    	getExchangeRate();
	    	$interval(function() {
	    		if(numberOfRetry < 3){
		    		//console.log("NUMBER OF RETRY WITHIN LIMITS------getExchangeRate--------->  ");
		 			getExchangeRate();
		 		}                	    	
	        }, 60 * 1000); 

		        function pad(n) {
		            return n<10 ? '0'+n : n;
		        }
		   	 	$('#navbar button[type="submit"]')
		   			.removeClass('btn-info')
		    		.addClass('btn-success')
		    		.text('Signed in ✔ '+username)
		    		.off('click');
		    	$('#navbar div.form-group').hide();
		    	$('#summary,#strategies').show();
		    	$('#pos').show(); 		
		    	},
		    headers: {
		        "Authorization": "Basic " + $rootScope.auth
		   	}
		});
	}
  }	
}

	 	$scope.openModal = function (size) {
	            $rootScope.modalInstance = $uibModal.open({
	            animation: $scope.animationsEnabled,
	            scope: $scope,
	            controller: 'PopupCont',
	            templateUrl: 'popup.html', 
	            size: size,
	            backdrop: 'static',
	            resolve: {
	                orderList: function () {
	                    return $scope.orderList;
	                }
	            }              
	        });
	    };

		

		/**
		* @name getDailyPL
		* @desc 
		* @param  
		* @returns returns DAILY p/L
	    **/
		function getDailyPL(){

		    var curr = new Date; // get current date
		 	var first = curr.getDate() - (curr.getDay()-1); // First day is the day of the month - the day of the week
			var endDate =  new Date().toISOString();
			curr.setHours(09);
	        curr.setMinutes(00);
	        curr.setSeconds(00);
			var firstday = new Date(curr.setDate(first)).toISOString();
			var startDate = new Date('2017-01-01T00:00:00.000')
			var endDate =  new Date().toISOString();
			$.ajax(__env.apiUrl+':'+__env.apiUrlPort+'/dailyPLs?startDate='+firstday+'&endDate='+endDate, {
				error: function(err) {
					console.log(err);
					update_in_progress = false;
				},
				success: function(data) {   
					dailyPLDataInDB = data;
				},
			    headers: {
				   'Content-Type': 'application/x-www-form-urlencoded'
				},
			}); 		
			
		}

		/**
		* @name getDailyPLForTotalNav
		* @desc 
		* @param  
		* @returns returns DAILY p/L
	    **/
		var getDailyPLForTotalNav = function(callback) {

       
                  var day;
                  var startDate;
                  var endDate;
                        
                  // To get the exchange rate for the previous day 17:30           
                  day = getDayOfTheWeek();    
                    if(day === "Monday"){
                        startDate = new Date();
                        startDate.setDate(startDate.getDate() - 3);       
                        endDate = new Date();
                        endDate.setDate(endDate.getDate() - 3);                             
                                      
                    }else{
                        startDate = new Date();
                        startDate.setDate(startDate.getDate() - 1);                             
                        endDate = new Date();
                        endDate.setDate(endDate.getDate() - 1);                       
                    }
                        startDate.setHours(08);
                        endDate.setHours(21);

                        // Then set minutes
                        startDate.setMinutes(00);
                        endDate.setMinutes(30);

                        // Then set seconds
                        startDate.setSeconds(00);
                        endDate.setSeconds(00);

                        var firstday = (startDate).toISOString();                
                        var endDate =  endDate.toISOString();     

                        

                          $http({
	                    url:__env.apiUrl+':'+__env.apiUrlPort+'/dailyPLs?startDate='+firstday+'&endDate='+endDate,
	                    method: "GET",                                         
	                    headers: {
	                         'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
	                    }
	                          
	                })
	                .then(function (data){
	                      
	                      console.log(data.data);
	                      dailyPrevDayPLDataInDB  = data.data;
	                      callback(dailyPrevDayPLDataInDB);

	                },function (error){
	                	console.log(error);

	                 });             
        }
		/**
		* @name getOrderHistory
		* @desc 
		* @param  
		* @returns returns DAILY p/L
	    **/
		function getOrderHistory() {
		    
	         var startDate = new Date();
	      	 // Set hours
	         startDate.setHours(08);
	         // Then set minutes
	         startDate.setMinutes(00);
	         // Then set seconds
	         startDate.setSeconds(00);
	      	 console.log("TIMEEEEEE getOrderHistorygetOrderHistorygetOrderHistorygetOrderHistorygetOrderHistory -->  " + startDate)
	         var unixTimeAtNine = Math.floor((startDate).getTime() / 1000);
	         var currentUnixTime = Math.floor((new Date()).getTime() / 1000);
	      
		$.ajax(__env.clientApiUrl+'/shadow_nav/order_history?startDate='+unixTimeAtNine+'&endDate='+currentUnixTime, {
	 		error: function(err) {
				console.log(err);
				update_in_progress = false;
			},
			success: function(data) {   				
				ordersPlaced = data;
				ordersPlaced.reverse();
				
			},
			headers: {
			    "Authorization": "Basic " + $rootScope.auth
			  	}
		});

	}



		/**
		* @name timeConverter
		* @desc 
		* @param  
		* @returns returns DAILY p/L
	    **/
  	function timeConverter(UNIX_timestamp){
          var a = new Date(UNIX_timestamp * 1000);
          var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
          var year = a.getFullYear();
          var month = months[a.getMonth()];
          var date = a.getDate();
          var hour = a.getHours();
          var min = a.getMinutes();
          var sec = a.getSeconds();
          var time = pad(date) + '/' + month + '/' + year  ;
          return time;
    }



		/**
		* @name pad
		* @desc 
		* @param  
		* @returns returns DAILY p/L
	    **/
    function pad(n) {
	    return n<10 ? '0'+n : n;
    }       
		/**
		* @name getHolidayList
		* @desc 
		* @param  
		* @returns returns DAILY p/L
	    **/
    function getHolidayList(holidayList) {
                
            $.ajax(__env.clientApiUrl+'/holidays?format=json', {
                error: function(err) {
                console.log(err);
                
            },
            success: function(data) {           
              
                console.log( data.Frankfurt_Stock_Exchange);
                data.Frankfurt_Stock_Exchange.forEach(function(item) {
                    holidayList.push(timeConverter(item));
            }); 

            console.log( holidayList.length);
            var DateOnly = new Date() 
            console.log(DateOnly);
            var dateTomm = (pad(DateOnly.getDate() +1 ) + '/' + pad(DateOnly.getMonth() + 1)+ '/' + DateOnly.getFullYear());
            console.log(dateTomm);
            for (var i = 0; i < holidayList.length; i++) {
                console.log(holidayList[i]);
                if(holidayList[i] == dateTomm){
                    console.log("TOMORROW IS A HOLIDAY _ NO TRADING DAY"); 
                    $scope.holidayMessage ="TOMORROW IS A HOLIDAY, NO TRADING DAY";
                }           
            } 
            return holidayList;                       
            },
            headers: {
                "Authorization": "Basic " + $rootScope.auth
                }
        });
    }
		/**
		* @name getOrderHistoryForOderConfirmation
		* @desc 
		* @param  
		* @returns returns DAILY p/L
	    **/
	 function getOrderHistoryForOderConfirmation() {
		    
	    var startDate = new Date();	    
	    // Then set minutes
	    startDate.setMinutes(startDate.getMinutes() - 2);	      
	    // console.log("TIMEEEEEE   " + startDate)
	    var unixTime = Math.floor((startDate).getTime() / 1000);
	    $.ajax(__env.clientApiUrl+'/shadow_nav/order_history', {
			error: function(err) {
				console.log(err);
				update_in_progress = false;
			},
			success: function(data) {   
				
				ordersForConfirmation = data;
				DataIn= true;
		
			},
			headers: {
			    "Authorization": "Basic " + $rootScope.auth
			}
		}); 
  
	 }
		 /**
		* @name calculateYesterdayClosePrice
		* @desc 
		* @param  
		* @returns returns DAILY p/L
	    **/
	   	  function calculateYesterdayClosePrice(title_id,name,$scope, $http){
		      	      	      	  
		     	
			// calculate the P/L for the getting the open value for a strategy at 9:00 AM 
			// API call for the above //Get the list of indexes per startegy to do the query //for ex: DAX it is 1000003
			//calculate the underlying open for a strartegy ..Open at the end of the day is the previous day closed value.
		    var day;
		    var startDate;
		    var endDate;
		    var error;
		    day = getDayOfTheWeek();    
	   
		    if(day === "Monday"){
		 	   startDate = new Date();
		       startDate.setDate(startDate.getDate() - 3);
			   endDate = new Date();
		       endDate.setDate(endDate.getDate() - 3);
		    }else{
		        startDate = new Date();
		  	   	startDate.setDate(startDate.getDate() - 1);
	       	    endDate = new Date();
		  	    endDate.setDate(endDate.getDate() - 1);
		    }
	       	startDate.setHours(17);
	        endDate.setHours(17);
            // Then set minutes
            startDate.setMinutes(28);
            endDate.setMinutes(30);
            // Then set seconds
            startDate.setSeconds(00);
            endDate.setSeconds(00);
            // console.log("TIMEEEEEE   " + startDate)
	        var unixTimeSTartTime = Math.floor((startDate).getTime() / 1000);
	        var unixTimeEndTime = Math.floor((endDate).getTime() / 1000);
			$.ajax(__env.clientApiUrl+'/titles/'+title_id+'/intraday?data_options=c&startDate='+unixTimeSTartTime+'&endDate='+unixTimeEndTime, {
				error: function(err) {
					console.log(err);
					error = err
					noDataFound = true;
	 				if((error.responseText).indexOf("No data found")) {
					    console.log("In error =========== "+ error.responseText); 
				 		//until we get the data for the previous day, Keep calling the API 
				 		var dataFound= calculateYesterdayClosePriceAgain(title_id,name,noDataFound);
					}
					update_in_progress = false;				 				
				},
				success: function(data) {   
	 				for (var fld in data.quotes[0]) {
						if (fld == 'c'){
						     if(name == "19"){
 						    	    closedPriceData.push([data.isin,data.quotes[0][fld]])
							 }else {
							    	closedPriceData.push([name,data.quotes[0][fld]])
							 }
						}	    						
					}				 				
				},
				headers: {
					    "Authorization": "Basic " + $rootScope.auth
				}
			}); 
		}	
	 	/**
		* @name calculateYesterdayClosePrice
		* @desc 
		* @param  
		* @returns returns DAILY p/L
	    **/
		
		function calculateYesterdayClosePriceAgain(title_id,name,noDataFound){
		      	      	      	  
		// calculate the P/L for the getting the open value for a strategy at 9:00 AM 
		// API call for the above //Get the list of indexes per startegy to do the query //for ex: DAX it is 1000003
		//calculate the underlying open for a strartegy ..Open at the end of the day is the previous day closed value.
		var day;
		var startDate;
		var endDate;
		var error;
		day = getDayOfTheWeek(); 
		if(noDataFound){
	   		 if(day === "Monday"){
	         	   startDate = new Date();
		           startDate.setDate(startDate.getDate() - 4);
		       	   endDate = new Date();
			       endDate.setDate(endDate.getDate() - 4);
		      }else{
		            if(day === "Tuesday"){
						startDate = new Date();
					   	startDate.setDate(startDate.getDate() - 5);
					    endDate = new Date();
					  	endDate.setDate(endDate.getDate() - 5);
	  		        }else{

						startDate = new Date();		
						startDate.setDate(startDate.getDate() - 2);
	   					endDate = new Date();
					  	endDate.setDate(endDate.getDate() - 2);
			        }
			  }
	          startDate.setHours(17);
		      endDate.setHours(17);
              // Then set minutes
              startDate.setMinutes(28);
              endDate.setMinutes(30);

		     // Then set seconds
		      startDate.setSeconds(00);
		      endDate.setSeconds(00);
	    }

	    var unixTimeSTartTime = Math.floor((startDate).getTime() / 1000);
	    var unixTimeEndTime = Math.floor((endDate).getTime() / 1000);
        $.ajax(__env.clientApiUrl+'/titles/'+title_id+'/intraday?data_options=c&startDate='+unixTimeSTartTime+'&endDate='+unixTimeEndTime, {
		error: function(err) {
			console.log(err);
			error = err
		if((error.responseText).indexOf("No data found")) {
			noDataFound = true;
		    console.log("In error ===========2nd TRY========= "+ name);
			dataFound = false;
			//until we get the data for the previous day, Keep calling the API 
			//dataFound= calculateYesterdayClosePriceAgain(title_id,name,Counter++,noDataFound);

		}
		update_in_progress = false;		
		return dataFound;		 				
		},
		success: function(data) {   
				
		dataFound = true;
		for (var fld in data.quotes[0]) {
			if (fld == 'c'){
				if( name == "19"){
				  	 closedPriceData.push([data.isin,data.quotes[0][fld]])
	  	     	}else {
  	 				closedPriceData.push([name,data.quotes[0][fld]])
			     }
			}	    						
		}				 				
		return dataFound;	
		},
		headers: {
		   "Authorization": "Basic " + $rootScope.auth
		}
	}); 
						}	
	/**
		* @name calculateYesterdayClosePrice
		* @desc 
		* @param  
		* @returns returns DAILY p/L
	    **/
		 function getExchangeRate() {
	 	     //Make the API calls and get the Exchange rate for FTSE stocks
			 //First, start with a particular time
	           var startDate = new Date();
	          startDate.setMinutes(startDate.getMinutes() - 40);
	          var currentUnixTime = Math.floor((startDate).getTime() / 1000);
	   	 
			// Call to get the current exchange rate
			$.ajax(__env.clientApiUrl+'/titles/99999/intraday?data_options=c&startDate='+currentUnixTime, {
				error: function(err) {
					console.log(err);
					update_in_progress = false;
				},
				success: function(data) {   
					//console.log(data);
					for (var fld in data.quotes[0]) {    					
						if (fld == 'c')
							exchangeRate= data.quotes[0][fld];
					}				
				//	console.log("NOWW    "+exchangeRate);
					},
				headers: {
				    "Authorization": "Basic " +$rootScope.auth
				  	}
			}); 		
	 	  return exchangeRate;
	   }
	  	     
	  	/**
		* @name calculateYesterdayClosePrice
		* @desc 
		* @param  
		* @returns returns DAILY p/L
	    **/ 	  
		  function getPrevExchangeRate(){
			  var startDate ;
		      var  endDate;    	      
		      var day;
			// To get the exchange rate for the previous day 17:30           
	         day = getDayOfTheWeek();                      
	        if(day === "Monday"){
	     	    startDate = getFridaysDate()+'T16:30';
	     	    endDate =getFridaysDate()+'T17:30';                    	
	      	  
	        }else{
	      	  startDate =getYesterdaysDate()+'T16:30';
	      	  endDate =getYesterdaysDate()+'T17:30';                  	  
	      	  
	        }
	        //call to get the previous day exchange rate
			$.ajax(__env.clientApiUrl+'/titles/99993/intraday?data_options=c&partition=hourly&startDate='+startDate+'&endDate='+endDate, {
			error: function(err) {
				console.log(err);
				update_in_progress = false;
			},
			success: function(data) {
				exchangePrevRateExists = true;
				//console.log(data);				
				for (var fld in data.quotes[0]) {
					
					if (fld == 'c'){
						exchangeRatePrev= data.quotes[0][fld];		    					
						}	    						
				}			    				
				//console.log("PREVVV    "+exchangeRatePrev);
			},
			headers: {
			    "Authorization": "Basic " + $rootScope.auth
			  	}
		});				
			return exchangeRatePrev;
			
		}
		
		/**
		* @name getYesterdaysDate
		* @desc 
		* @param  
		* @returns returns DAILY p/L
	    **/
		function getYesterdaysDate() {
	   	 
		    $today = new Date();
		    $yesterday = new Date($today);
		    $yesterday.setDate($today.getDate() - 1);
		    
		    var $dd = $yesterday.getDate();
		    var $mm = $yesterday.getMonth()+1; //January is 0!

		    var $yyyy = $yesterday.getFullYear();
		    if($dd<10){$dd='0'+$dd} if($mm<10){$mm='0'+$mm} $yesterday = $yyyy+'-'+$mm+'-'+$dd;
		    return $yesterday ;
		}
		/**
		* @name getFridaysDate
		* @desc 
		* @param  
		* @returns returns DAILY p/L
	    **/		
		function getFridaysDate() {
		    
		    $today = new Date();
		    $yesterday = new Date($today);
		    $yesterday.setDate($today.getDate() - 3);
		    
		    var $dd = $yesterday.getDate();
		    var $mm = $yesterday.getMonth()+1; //January is 0!

		    var $yyyy = $yesterday.getFullYear();
		    if($dd<10){$dd='0'+$dd} if($mm<10){$mm='0'+$mm} $yesterday = $yyyy+'-'+$mm+'-'+$dd;
		    return $yesterday ;
		}

		/**
		* @name getDayOfTheWeek
		* @desc 
		* @param  
		* @returns returns DAILY p/L
	    **/	
		
		function getDayOfTheWeek() {
		    var d = new Date();
		    var weekday = new Array(7);
		    weekday[0] = "Sunday";
		    weekday[1] = "Monday";
		    weekday[2] = "Tuesday";
		    weekday[3] = "Wednesday";
		    weekday[4] = "Thursday";
		    weekday[5] = "Friday";
		    weekday[6] = "Saturday";
		    return weekday[d.getDay()];
		    
		}

	/**
	* @name updateDashboard
	* @desc 
	* @param  
	* @returns returns DAILY p/L
	**/	
	function updateDashboard($scope, $http) {
		//console.log("IN DASHBOARD" +$scope.IsloggedIn);	
		if (update_in_progress) return;
		update_in_progress = true;
		// To get the previous day Exchange rate, check at 18:30 Everyday except Saturday and sunday. 
		//At this time the exchanage rate is recorded so that we dont have to make the call everyfew second.

		//check if the current time is 18:30 
	   	if(  ((new Date().getHours() + ":" + new Date().getMinutes()) == "18:30") || !exchangePrevRateExists){
	   	//	console.log("IN getPrevExchangeRate calling  "+exchangePrevRateExists );
		    exchangeRatePrev = getPrevExchangeRate();
	       				
		}	
		$.ajax(__env.clientApiUrl+'/shadow_nav/?dev_call='+dev_call, {
		    error: function(err) { 
			console.log(err);
			numberOfRetry++;
			update_in_progress = false;
			if(numberOfRetry >2) {
				console.log("numberOfRetry -----  "+numberOfRetry );
					emailjs.init("user_rRhmrTy6ydBNGpItVhiH3");
			    	emailjs.send("mailjet","template2",{email: err.responseText})
			                 .then(function(response) {
			                   console.log("SUCCESS. status=%d, text=%s", response.status, response.text);
			                 
			                 })
	         }
		},
		success: function(data) {
		console.log("IN HERE");
			console.log(data);
			$scope.reportItems = data;		
			$scope.strategyData= $scope.reportItems.strategies;
		//	console.log($scope.strategyData);
			calculateSummary( $scope.strategyData,$scope, $http);
		},headers: {
		    "Authorization": "Basic " + $rootScope.auth
		  	}
	});
			
	 update_in_progress = false;
	}

	/**
	* @name calculateSummary
	* @desc 
	* @param  
	* @returns returns DAILY p/L
	**/	
	function calculateSummary(strategyData,$scope, $http){
		$scope.navwithoutFTSE =0;
		$scope.initial_navwithoutFTSE= 0;
		$scope.navwithFTSE = 0;
		$scope.initial_navwithFTSE = 0;
		$scope.nav=0;
		$scope.initial_nav=0;
		$scope.weeklyPL= 0;
		$scope.exposure= 0;
		$scope.updated_at=0;
		$scope.gain_loss = 0;	
		$scope.strategyTable=[] ;
		$scope.positionsData=[] ;
		$scope.positionsDataVOPA=[];		
		
		for (var strategy in strategyData) {
			 if(strategy === "Cash"  || strategy === "Catana Virtual MDAX Intensity")
			 { continue; }
			   //console.log((strategyData[strategy].trader));	
			   if(!(strategy === "Catana_FTSE_Future")){
				   $scope.navwithoutFTSE += strategyData[strategy].nav;    					
				   $scope.initial_navwithoutFTSE += strategyData[strategy].prev_nav ? strategyData[strategy].prev_nav : 0;					
			    }else {			    	
			    	$scope.navwithFTSE += strategyData[strategy].nav;    					
			    	$scope.initial_navwithFTSE += strategyData[strategy].prev_nav ? strategyData[strategy].prev_nav : 0;
			    }				   
			   $scope.nav +=strategyData[strategy].nav;
			   $scope.cash += strategyData[strategy].cash;
			   $scope.initial_nav += strategyData[strategy].prev_nav ? strategyData[strategy].prev_nav : 0;				
			   $scope.weeklyPL +=strategyData[strategy].prev_week_nav;
			   $scope.strategyTable =strategyData[strategy];
		
			if(strategyData[strategy].positions.length > 0 && strategyData[strategy].trader != "Catana VOPA Trader"){
				$scope.positionsData.push(strategyData[strategy].positions);
			}		
			 
	   }		
		$scope.update_at = new Date();	
		var summarycurrentNav = ($scope.navwithFTSE * exchangeRate) + $scope.navwithoutFTSE ;			
	    var summaryprevNav = ($scope.initial_navwithFTSE * exchangeRatePrev) + $scope.initial_navwithoutFTSE;
	    $scope.gain_loss =  ((Math.round((summarycurrentNav / summaryprevNav -1)*10000)/100)+'%');
	    $scope.weeklyPL = ((Math.round(($scope.nav / $scope.weeklyPL -1)*10000)/100)+'%');
	    $scope.summaryNav = $scope.nav;
	    getPositions($scope.positionsData,totalNAV,$scope, $http);	   
	}

	/**
	* @name getClosePrice
	* @desc 
	* @param  
	* @returns returns DAILY p/L
	**/	
	function getClosePrice(name){
		var closePrice ;
		for(var field in closedPriceData){
			var str = ''+closedPriceData[field]+'';
		 	var match = str.split(',');
		   	//console.log(match)
		   	for(var i=0;i< match.length;i++){
		   		if(name == (match[i])){
		   			//if(match[i] == ordersPlaced[orders].name) {
		  			if (name.substring) {
		   				// do string thing
					} else{
		  				// do other thing
					}
		    		closePrice = match[i+1];
		    		//console.log("closePrice Without trading ----> "+ match[i+1] );
		    		break;
		    	}	 									    	
		    } 
		}
		return closePrice;
	}

	/**
	* @name getCurrentPrice
	* @desc 
	* @param  
	* @returns returns DAILY p/L
	**/	
	function getCurrentPrice(name){
		//console.log("getCurrentPrice FUNCTION ---->"+ name); 
		var currentPrice ;
		for(var field in currrentPriceFromTickData){
		   	if (name == currrentPriceFromTickData[field].title_id) {
				currentPrice = currrentPriceFromTickData[field].currentPrice;
		    	//	console.log("currentPrice Without trading ----> "+currentPrice );
	    		break;
			}
		}			
		return currentPrice;
	}

	/**
	* @name getUpdatedDate
	* @desc 
	* @param  
	* @returns returns DAILY p/L
	**/	
	function getUpdatedDate(name){
		//	console.log("getUpdatedDate FUNCTION ---->"+ name); 
		var date  ;
		for(var field in currrentPriceFromTickData){
		   	if (name == currrentPriceFromTickData[field].title_id) {
				date = currrentPriceFromTickData[field].updated_at;
		    	//	console.log("getUpdatedDate Without trading ----> "+date );
	    		break;
			}
		}			
		return date;
	}


	/**
	* @name getPositions
	* @desc 
	* @param  
	* @returns returns DAILY p/L
	**/
	function getPositions(positionsData,totalNAV,$scope, $http){
		position_ids = [];	
		$scope.strategyName = [];
		$scope.pos= [];	
		$scope.exposureVal ;
		$scope.PandLNeutralStok=0;
		$scope.PandLNeutralStockStrategy=0;
		$scope.PandLLongStok=0;
		$scope.PandLRetailStok=0;
		$scope.stocksNeutralPositions = false;
		$scope.stocksLongPositions = false;
		$scope.stocksRetailPositions = false;
		$scope.mdaxPositions =false;
		$scope.stocksNeutralGainLoss;
		$scope.stocksNeutralOpenPrice;
		$scope.stocksNeutralCurrentPrice;
		$scope.stocksLongGainLoss;
		$scope.stocksLongOpenPrice;
		$scope.stocksLongCurrentPrice;
		$scope.stocksRetailGainLoss;
		$scope.stocksRetailOpenPrice;
		$scope.stocksRetailCurrentPrice;
		$scope.summaryPandLMDAXIncep = 0 ;
		var currrentPriceNeutral=0;
		var exp = 0;
		var StocksNuetralopen1 = 0;
	    var StocksNuetralopen2 = 0;
	    var StocksNuetralcurrent1 = 0;
	    var stocksNuetralCurrentSummary = 0;
	    var stocksNuetralYesterdayCloseSummary= 0;
	    var StocksNuetralcurrent2 = 0;
	    var StocksLongopen1 = 0;
	    var StocksLongopen2 = 0;
	    var StocksLongcurrent1 = 0;
	    var StocksLongcurrent2 = 0;
	    var dailyValStocksNuetral=0;
	    var dailyValStocksLong=0;
	    var StocksRetailopen1 = 0;
	    var StocksRetailopen2 = 0;
	    var StocksRetailcurrent1 = 0;
	    var StocksRetailcurrent2 = 0;
	    var dailyValStocksRetail=0;
	    $scope.dailyPandLTableRetail = [];
	    $scope.dailyPandLTable = [];
	   // $scope.dailyPandLDAX =[];
	    $scope.dailyPandLDAX= [];
	    var dataBeforeCloseOrder = [];
	    var gain_loss_default= "n/a";
	    var SummaryPandLMDAX= 0;
	    $scope.displaySummaryPandLMDAX= 0;
	    $scope.totPL = 0;      
	    var totalPandL= 0;
	    var totalPandLNew= 0;   
	    var totalCurrent = 0;
	    $scope.totalSummaryNav=0 ;
	    var dailyPrevDayPLDataInDB= [];
	    $scope.dailyPandLDAX.push({ strategy_id: "1",strategy:"Catana DAX SMA 15 + Gold", gain_loss:gain_loss_default,title_id:4000003 });
	    $scope.dailyPandLDAX.push({ strategy_id: "3",strategy:"Catana DAX Intensity", gain_loss:gain_loss_default, title_id:4000003 });
	    $scope.dailyPandLDAX.push({ strategy_id: "13",strategy:"Catana DAX SMA", gain_loss:gain_loss_default, title_id:4000003 });    
	    $scope.dailyPandLDAX.push({ strategy_id: "99",strategy:"IB TEST TRADER", gain_loss:gain_loss_default, title_id:99999 });
	    $scope.dailyPandLDAX.push({ strategy_id: "98",strategy:"IB TEST TRADER 2", gain_loss:gain_loss_default, title_id:99999 });
	    // $scope.dailyPandLDAX.push({ strategy_id: "19",strategy:"Catana Retail Strategy", gain_loss:gain_loss_default, title_id:1000005 });	   
	    $scope.dailyPandLDAX.push({ strategy_id: "97",strategy:"IB TEST TRADER 3", gain_loss:gain_loss_default, title_id:99999 });
	    $scope.dailyPandLDAX.push({ strategy_id: "22",strategy:"Catana DAX SMA 28 + Gold", gain_loss:gain_loss_default, title_id:4000003 });
	    $scope.dailyPandLDAX.push({ strategy_id: "24",strategy:"Catana DAX SMA 26", gain_loss:gain_loss_default, title_id:4000003 });	    
	    $scope.dailyPandLDAX.push({ strategy_id: "25",strategy:"Catana DAX SMA 20 + Gold", gain_loss:gain_loss_default, title_id:4000003 });
	    $scope.dailyPandLDAX.push({ strategy_id: "26",strategy:"Catana DAX SMA 30", gain_loss:gain_loss_default, title_id:4000003 });
	    $scope.dailyPandLDAX.push({ strategy_id: "29",strategy:"Catana DM", gain_loss:gain_loss_default, title_id:4000003 });

	    $scope.dailyPandLDAX.push({ strategy_id: "95",strategy:"IB TEST Anlagegrenzprüfung", gain_loss:gain_loss_default, title_id:4000003 });
	    $scope.dailyPandLDAX.push({ strategy_id: "94",strategy:"Catana External Orders Test", gain_loss:gain_loss_default, title_id:99998 });
	  
	    
	 
	    
		//Get the orer History to compare with positions
		if(!oderHistoryTableChanged)
	   		getOrderHistory();
		    for (var position in $scope.dailyPandLDAX) {
			 	  
	         	// To get the previous day close price, check at 18:30 Everyday except Saturday and sunday. 
		    	//At this time the exchange rate is recorded so that we dont have to make the call everyfew second.
		    	//check if the current time is 18:30 	    				
		    	if(  ((new Date().getHours() + ":" + new Date().getMinutes()) == "18:30") || !yesterdayClosePriceExists){
		    		calculateYesterdayClosePrice( $scope.dailyPandLDAX[position].title_id,$scope.dailyPandLDAX[position].strategy_id,$scope, $http);
	    		}		  	    			
			}        
		angular.forEach(positionsData, function(obj, key) {
	  	//  console.log(key + ': ' + obj);			
	    for (var position in obj) {
  			 if((obj[position].strategy_id == 1) || (obj[position].strategy_id == 13 ) ||	(obj[position].strategy_id == 25)||	(obj[position].strategy_id == 26) ||	(obj[position].strategy_id == 29) ||	(obj[position].strategy_id == 22) ||	(obj[position].strategy_id == 24)	|| 	(obj[position].strategy_id == 98 ) ||	(obj[position].strategy_id == 99) ||	(obj[position].strategy_id == 97) ||	(obj[position].strategy_id == 94) ||	(obj[position].strategy_id == 95)  ){  			
				$scope.mdaxPositions = true;
  			   	// When there is a position, check if there is any order history for that strategy for that time	    			   	
  			   	var positionNotInOrder = true;
   			    var gain_loss=0;
   			    var closePriceOfPrevOpenedLongPosition = 0;
   			    var closePriceOfPrevOpenedShortPosition = 0;
   			    //get the Current price in the particular order(Execution price) from the tick Data of websoscket, if its not available then take from the position .
				var currentPrice = getCurrentPrice(obj[position].title_id);
				if(currentPrice == null){
			//		console.log("IN currentPrice------ NULL---------->"+currentPrice);
				    currentPrice = obj[position].current_price
		    	}
		    	var updated_at = getUpdatedDate(obj[position].title_id);
		    //	console.log("IN updated_at------BEFORE---------->"+updated_at);
				if(updated_at == null)
		    	    updated_at = obj[position].updated_at
		    //	console.log("IN updated_at----------AFTER------>"+updated_at);
				// to calculate the P/L for the strategies:
		    	//First get the Strategyname and search in the Query from the Order history API call. 
		    	for (var orders in ordersPlaced)    {	
		    		if(obj[position].strategy == ordersPlaced[orders].name && ordersPlaced[orders].confirmed_at != null )  {
		    			positionNotInOrder= false;
		    		    //get the order type. If it is close then P/L is  current price in Orderhistory/yesterday close at 17:30
		    			var closePrice = getClosePrice(obj[position].strategy_id);
			    		if(ordersPlaced[orders].type == "CLOSE"){
		    				//console.log("When order is closed- but there exists a open position further---->_________type == CLOSE___________>" +ordersPlaced[orders].name);
		    			 	//Long Position closed
		    				if(ordersPlaced[orders].size <  0 ){
		    					//console.log("When order is closed- but there exists a open position further---->_________Long Position closed__________>" +ordersPlaced[orders].name);
		    					if(ordersPlaced[orders].average_price != null){
		    												 //In case of multitrade scenario whenthe Long position is closed ,Check if the previous long position  was opened on the same day
		    												 if(closePriceOfPrevOpenedLongPosition != 0){
		    													 gain_loss= ((ordersPlaced[orders].average_price/closePriceOfPrevOpenedLongPosition));
		    												//	 console.log("closePriceOfPrevOpenedLongPosition______Long Position closed______closePriceOfPrevOpenedLongPosition____________>" +closePriceOfPrevOpenedLongPosition+" ----- "+ gain_loss);
		    												 }
		    												  
		    												 else {

		    													 gain_loss= ((ordersPlaced[orders].average_price/closePrice));
		    												//	 console.log("No Long Position opened on the same day___go from short to long scenario____________>" + gain_loss);
		    													 }
		    											 }else {
		    											 	//When the average price is not yet generated in the order_history table_
		    												gain_loss= ((currentPrice/closePrice));
		    												//console.log("When the average price is not yet generated in the order_history table______Long Position closed______>" + gain_loss);
		    											 }
		    											 
		    											// console.log("ENd of the closed loop __________Long Position closed_________________>" +ordersPlaced[orders].name+" ----- "+ gain_loss);	    										 
		    											
		    											 //Check if the order is already present  in the positions table, for the multiple trading in a day. 
		    											 // If this is already present, then account for the P/L and delete this strategy position and add new.
		    											 //Formula for Multiple P/L is (existingP/L * (curr/avgprice) -1) ,Similarly for all below
		    											 var prevPositionPL = CheckIfPositionPresent($scope.strategyName,ordersPlaced[orders].name,ordersPlaced[orders].type);
		    											// console.log(" CHECK if the previous position existsed to account for P/L----------prevPositionPL **** > "+prevPositionPL);
		    											 if(prevPositionPL != null){
		    												 gain_loss = (prevPositionPL * gain_loss ) ;
		    												  updateData($scope.dailyPandLDAX,ordersPlaced[orders].name,Math.round((gain_loss-1)*10000)/100+'%' )
		    												  $scope.strategyName.push({strategyNewName: replaceStrategyName(obj[position].strategy), rawPL:parseFloat(gain_loss), positiondata: obj[position],currentPrice: currentPrice, next_stop:(Math.round(((currentPrice -obj[position].next_stop)/currentPrice)*10000)/100) ,updated_at :updated_at, gain_loss_today:Math.round(parseFloat(gain_loss-1)*10000)/100+'%' ,gain_loss: Math.round(parseFloat(obj[position].gain_loss)*10000)/100+'%' });
		    												 
		    												//  console.log(" strategyName.push&&&&&&&&&&&&&&&&&&&&&&&&&&&&----------prevPositionPL------1----> "+obj[position].size);
		    												//  console.log("previous position existsed _____Long Position closed___prevPositionPL_______________>" +ordersPlaced[orders].name+" ----- "+ gain_loss);	
		    													    												 
		    											 }  else{
		    											 	//console.log("previous position NOT existsed _____Long Position closed___NO prevPositionPL_______________>" +ordersPlaced[orders].name+" ----- "+ gain_loss);	
		    												 updateData($scope.dailyPandLDAX,ordersPlaced[orders].name,Math.round((gain_loss-1)*10000)/100+'%' )
		    												 $scope.strategyName.push({strategyNewName: replaceStrategyName(obj[position].strategy), rawPL:parseFloat(gain_loss), positiondata: obj[position],currentPrice: currentPrice, next_stop:(Math.round(((currentPrice -obj[position].next_stop)/currentPrice)*10000)/100) ,updated_at :updated_at,gain_loss_today:Math.round(parseFloat(gain_loss-1)*10000)/100+'%' ,gain_loss: Math.round(parseFloat(obj[position].gain_loss)*10000)/100+'%' });
		    												 
		    												// console.log(" strategyName.push&&&&&&&&&&&&&&&&&&&&&&&&&&&&----------prevPositionPL------2----> "+obj[position].size);
		    												 
		    											  }
		    											
		    											//Short Position closed
		    										 }else if(ordersPlaced[orders].size > 0) {
		    										 	//console.log("When order is closed- but there exists a open position further---->_________type == CLOSE___________>" +ordersPlaced[orders].name);
		    											 if(ordersPlaced[orders].average_price != null){
		    												  //In case of multitrade scenario whenthe Short position is closed ,Check if the previous Short position  was opened on the same day
		    												 if(closePriceOfPrevOpenedShortPosition != 0){
		    												 	 
		    												     gain_loss= ((closePriceOfPrevOpenedShortPosition/ordersPlaced[orders].average_price));
		    												  //   console.log("closePriceOfPrevOpenedShortPosition______Short Position closed______closePriceOfPrevOpenedShortPosition____________>" +closePriceOfPrevOpenedShortPosition+" ----- "+ gain_loss);
		    												     }else{
		    												    	 gain_loss= ((closePrice/ordersPlaced[orders].average_price));
		    												   // 	 console.log("No Short Position opened on the same day___go from Long to Short scenario____________>" + gain_loss);
		    												     }
		    											 }else{
		    												 gain_loss= ((closePrice/currentPrice));
		    												// console.log("When the average price is not yet generated in the order_history table___Short Position closed__________>" + gain_loss);
		    											 }
		    													    												    									
		    											 // console.log("ENd of the closed loop __________Short Position closed_________________>" +ordersPlaced[orders].name+" ----- "+ gain_loss);	
		    											 //Check if the order is already present  in the positions table, for the multiple trading in a day. 
		    											 // If this is already present, then account for the P/L and delete this strategy position and add new.
		    											 //Formula for Multiple P/L is (existingP/L * (curr/avgprice) -1) ,Similarly for all below
		    											 var prevPositionPL = CheckIfPositionPresent($scope.strategyName,ordersPlaced[orders].name,ordersPlaced[orders].type);
		    											// console.log(" CHECK if the previous position existsed to account for P/L----------prevPositionPL **** > "+prevPositionPL);
		    											 if(prevPositionPL != null){
		    												  gain_loss = (prevPositionPL * gain_loss );
		    												  updateData($scope.dailyPandLDAX,ordersPlaced[orders].name,Math.round((gain_loss-1)*10000)/100+'%' )
		    												  $scope.strategyName.push({strategyNewName: replaceStrategyName(obj[position].strategy), rawPL:parseFloat(gain_loss), positiondata: obj[position],currentPrice: currentPrice, next_stop:(Math.round(((currentPrice -obj[position].next_stop)/currentPrice)*10000)/100) ,updated_at :updated_at,gain_loss_today:Math.round(parseFloat(gain_loss-1)*10000)/100+'%' ,gain_loss: Math.round(parseFloat(obj[position].gain_loss)*10000)/100+'%' });
		    												
		    												//  console.log("previous position existsed _____Short Position closed___prevPositionPL_______________>" +ordersPlaced[orders].name+" ----- "+ gain_loss);
		    												//  console.log(" strategyName.push&&&&&&&&&&&&&&&&&&&&&&&&&&&&----------prevPositionPL------3----> "+ obj[position].size);	
		    												 
		    											 }  else{
		    												 updateData($scope.dailyPandLDAX,ordersPlaced[orders].name,Math.round((gain_loss-1)*10000)/100+'%' )
		    												  $scope.strategyName.push({strategyNewName: replaceStrategyName(obj[position].strategy), rawPL:parseFloat(gain_loss), positiondata: obj[position],currentPrice: currentPrice,next_stop:(Math.round(((currentPrice -obj[position].next_stop)/currentPrice)*10000)/100) , updated_at :updated_at,gain_loss_today:Math.round(parseFloat(gain_loss-1)*10000)/100+'%' ,gain_loss: Math.round(parseFloat(obj[position].gain_loss)*10000)/100+'%' });
		    													
		    												//console.log("previous position NOT existsed--------- Short Position closed___NO prevPositionPL___________>" + gain_loss);
		    												//console.log(" strategyName.push&&&&&&&&&&&&&&&&&&&&&&&&&&&&----------prevPositionPL------4----> "+ obj[position].size);
		    											 }
		    											 
		    										 }   									
		    								}else{
		    									//Long Position opened	
		    									 if(ordersPlaced[orders].size > 0 ){
		    									 	//console.log("Long Position opened---->_________type == OPEN___________>" +ordersPlaced[orders].name);
		    										 if(ordersPlaced[orders].average_price != null){
		    										 	//In case of multitrading scenario, when the position is opened, check if the position is going to closed the same day, 
		    										 	//as the current avg price will be the close price of this position and not the prev day 17:30 price.
		    											 if(isStrategyClosed(ordersPlaced,ordersPlaced[orders].size,ordersPlaced[orders].name,ordersPlaced[orders].type,ordersPlaced[orders].completed_at)) {
		    												 closePriceOfPrevOpenedLongPosition= ordersPlaced[orders].average_price;
		    												// console.log("isStrategyClosed_____________________LONG Position opened-------------------->there exixts a close ");
		    												 continue;
		    											 }else {
		    											 	//if the position is not going to be closed the same day then normal calculation
		    												 gain_loss = ((currentPrice/ordersPlaced[orders].average_price));
		    												//  console.log("Long position open scenario____________>" + gain_loss);
		    											 }
		    										 }else{
		    											 gain_loss =  ((currentPrice/currentPrice));
		    											//  console.log("When the average price is not yet generated in the order_history table____Long Position opened_________>" + gain_loss);
		    										 }
		    										// console.log("ENd of the closed loop __________Long Position opened_________________>" +ordersPlaced[orders].name+" ----- "+ gain_loss);
		    										 //Check if the order is already present  in the positions table, for the multiple trading in a day. 
		    										 // If this is already present, then account for the P/L and delete this strategy position and add new.
		    										 //Formula for Multiple P/L is (existingP/L * (curr/avgprice) -1) ,Similarly for all below
		    										 var prevPositionPL = CheckIfPositionPresent($scope.strategyName,ordersPlaced[orders].name,ordersPlaced[orders].type);
		    										// console.log(" CHECK if the previous position existsed to account for P/L----------prevPositionPL **** > "+prevPositionPL);
			    									 if(prevPositionPL != null){
			    										  gain_loss = (prevPositionPL * gain_loss );
			    										  updateData($scope.dailyPandLDAX,obj[position].strategy,Math.round(parseFloat(gain_loss-1)*10000)/100+'%' );
			    										  $scope.strategyName.push({strategyNewName: replaceStrategyName(obj[position].strategy), rawPL:parseFloat(gain_loss), positiondata: obj[position],currentPrice: currentPrice,next_stop:(Math.round(((currentPrice -obj[position].next_stop)/currentPrice)*10000)/100) ,updated_at :updated_at,gain_loss_today:Math.round(parseFloat(gain_loss-1)*10000)/100+'%' ,gain_loss: Math.round(parseFloat(obj[position].gain_loss)*10000)/100+'%' });
			    										
			    										//  console.log("previous position existsed _____Long Position opened___prevPositionPL_______________>" +ordersPlaced[orders].name+" ----- "+ gain_loss);
			    										//  console.log(" strategyName.push&&&&&&&&&&&&&&&&&&&&&&&&&&&&----------prevPositionPL------5----> "+ obj[position].size);
			    									 }  else{
			    										 updateData($scope.dailyPandLDAX,obj[position].strategy,Math.round(parseFloat(gain_loss-1)*10000)/100+'%' );
			    										 $scope.strategyName.push({strategyNewName: replaceStrategyName(obj[position].strategy), rawPL:parseFloat(gain_loss), positiondata: obj[position],currentPrice: currentPrice, next_stop:(Math.round(((currentPrice -obj[position].next_stop)/currentPrice)*10000)/100) ,updated_at :updated_at,gain_loss_today:Math.round(parseFloat(gain_loss-1)*10000)/100+'%' ,gain_loss: Math.round(parseFloat(obj[position].gain_loss)*10000)/100+'%' });
			    										 
			    										 //console.log("previous position NOT existsed---------Long Position opened___NO prevPositionPL___________>" + gain_loss);
			    										 //console.log(" strategyName.push&&&&&&&&&&&&&&&&&&&&&&&&&&&&----------prevPositionPL------6----> "+ obj[position].size);
			    									  }
		    										
			    									//Short Position opened	
		    									 }else if(ordersPlaced[orders].size < 0) {
		    										 if(ordersPlaced[orders].average_price != null){
		    											 //In case of multitrading scenario, when the position is opened, check if the position is going to closed the same day, 
		    										 	//as the current avg price will be the close price of this position and not the prev day 17:30 price.
			    											 if(isStrategyClosed(ordersPlaced,ordersPlaced[orders].size,ordersPlaced[orders].name,ordersPlaced[orders].type,ordersPlaced[orders].completed_at)) {
			    												 closePriceOfPrevOpenedShortPosition= ordersPlaced[orders].average_price;
			    												// console.log("isStrategyClosed_____________________SHORT Position opened-------------------->there exixts a close ");
			    												 continue;
			    											 }else {
			    											 	//if the position is not going to be closed the same day then normal calculation
			    												 gain_loss = ((ordersPlaced[orders].average_price/currentPrice));
			    												// console.log("Short position open scenario____________>" + gain_loss);
			    											 }
		    										 }else {
		    											 gain_loss = ((currentPrice/currentPrice));
		    											// console.log("When the average price is not yet generated in the order_history table_____SHORT Position opened_______>" + gain_loss);
		    										 }
		    										  //console.log("ENd of the closed loop __________SHORT Position opened_________________>" +ordersPlaced[orders].name+" ----- "+ gain_loss);
		    										 //Check if the order is already present  in the positions table, for the multiple trading in a day. 
		    										 // If this is already present, then account for the P/L and delete this strategy position and add new.
		    										 //Formula for Multiple P/L is (existingP/L * (curr/avgprice) -1) ,Similarly for all below
		    										 var prevPositionPL = CheckIfPositionPresent($scope.strategyName,ordersPlaced[orders].name,ordersPlaced[orders].type);
		    										// console.log(" CHECK if the previous position existsed to account for P/L----------prevPositionPL **** > "+prevPositionPL);
			    									 if(prevPositionPL != null){
			    										  gain_loss = (prevPositionPL * gain_loss );
			    										  updateData($scope.dailyPandLDAX,obj[position].strategy,Math.round(parseFloat(gain_loss-1)*10000)/100+'%' );
			    										  $scope.strategyName.push({strategyNewName: replaceStrategyName(obj[position].strategy), rawPL:parseFloat(gain_loss), positiondata: obj[position],currentPrice: currentPrice, next_stop:(Math.round(((currentPrice -obj[position].next_stop)/currentPrice)*10000)/100) ,updated_at :updated_at,gain_loss_today:Math.round(parseFloat(gain_loss-1)*10000)/100+'%' ,gain_loss: Math.round(parseFloat(obj[position].gain_loss)*10000)/100+'%' });
			    										 
			    										 // console.log("previous position existsed _____SHORT Position opened___prevPositionPL_______________>" +ordersPlaced[orders].name+" ----- "+ gain_loss);
			    										 // console.log(" strategyName.push&&&&&&&&&&&&&&&&&&&&&&&&&&&&----------prevPositionPL------7----> "+ obj[position].size);
			    									 }  else{
			    										 
			    		    								
			    										 updateData($scope.dailyPandLDAX,obj[position].strategy,Math.round(parseFloat(gain_loss-1)*10000)/100+'%' );
			    										 $scope.strategyName.push({strategyNewName: replaceStrategyName(obj[position].strategy), rawPL:parseFloat(gain_loss), positiondata: obj[position],currentPrice: currentPrice,next_stop:(Math.round(((currentPrice -obj[position].next_stop)/currentPrice)*10000)/100) , updated_at :updated_at,gain_loss_today:Math.round(parseFloat(gain_loss-1)*10000)/100+'%' ,gain_loss: Math.round(parseFloat(obj[position].gain_loss)*10000)/100+'%' });
			    										
			    										// console.log("previous position NOT existsed---------SHORT Position opened___NO prevPositionPL___________>" + gain_loss);
			    										// console.log(" strategyName.push&&&&&&&&&&&&&&&&&&&&&&&&&&&&----------prevPositionPL------8----> "+obj[position].size);
			    									  }
		    										
		    									 }   							
		    						        }
		    								//console.log("--------------END OF LOOP WHEN ORDER:NAME == POSITION:NAME__________________________>" +ordersPlaced[orders].name+" ----- "+ (gain_loss));
		    								
		    								
		    				      }else if(ordersPlaced[orders].name !== obj[position].strategy && ordersPlaced[orders].confirmed_at != null && ordersPlaced[orders].name != "IB TEST Retail Strategy"){
											 if((ordersPlaced[orders].type == "CLOSE") && ordersPlaced[orders].strategy_id == "19" ){

		    				      			//	console.log("--------------START OF LOOP WHEN ORDER:NAME != POSITION:NAME___________SPECIAL_CATANA_LONG___________ORDER____>" +ordersPlaced[orders].name+" ---POSITION-- "+obj[position].strategy);
			    				    	   		var closePrice = getClosePrice(ordersPlaced[orders].isin);									   
			    								
			    								if(ordersPlaced[orders].type == "CLOSE"){
			    									 if(ordersPlaced[orders].size < 0 ){	
			    										 if(ordersPlaced[orders].average_price != null){
			    											 gain_loss_today =ordersPlaced[orders].average_price/closePrice; 
			    											 dailyValStocksRetail+=((Math.abs(ordersPlaced[orders].size)*closePrice)/$scope.stocksRetailCurrentPrice)*((ordersPlaced[orders].average_price/closePrice)-1);

			    										 }else {
			    											 gain_loss_today =closePrice/closePrice;
			    											 dailyValStocksRetail+=((Math.abs(obj[position].size)*closePrice)/$scope.stocksRetailCurrentPrice)*((closePrice/closePrice)-1);
			    										 }
			    										//TO-DO: Multitrading scenario for LONG and Nuetral STOCKS:		    									
				    								/*	 var prevPositionPL = CheckIfStocksPositionPresent($scope.dailyPandLTable,obj[position].en);
				    									 if(prevPositionPL != null){
				    										 gain_loss_today = (prevPositionPL * gain_loss_today );
				    										 $scope.dailyPandLTable.push({ strategy: obj[position].strategy, rawPL:parseFloat(gain_loss_today-1),en: obj[position].en, isin: obj[position].isin, position: obj[position].position, size:  obj[position].size,  open_price: obj[position].open_price, current_price: obj[position].current_price, gain_loss: (Math.round(obj[position].gain_loss*10000)/100)+'%', gain_loss_today:  Math.round(parseFloat(gain_loss_today -1)*10000)/100+'%', updated_at: obj[position].updated_at });
				    									 }else {*/
				    										// $scope.dailyPandLTable.push({ strategy: obj[position].strategy, rawPL:parseFloat(gain_loss_today-1),en: ordersPlaced[orders].en, isin: ordersPlaced[orders].isin, position: obj[position].position, size:  obj[position].size,  open_price: obj[position].open_price, current_price: obj[position].current_price, gain_loss: (Math.round(obj[position].gain_loss*10000)/100)+'%', gain_loss_today:  Math.round(parseFloat(gain_loss_today -1)*10000)/100+'%', updated_at: obj[position].updated_at });
				    										// console.log("totalPandLNew__________VERY IMPORTANT_____2____________>" +ordersPlaced[orders].en+" ----- "+ ((Math.round(parseFloat(gain_loss_today -1)*10000)/100)) +" -------------------closePrice-------"+closePrice);
				    										
				    									// }
				    						  		}	

				    						   }
				    						   //Delete that Entry in order table, so that Next time it is not accounted for.
				    						    	var index = ordersPlaced.indexOf(ordersPlaced[orders]);
					    				    			  //console.log("index -------------------------index "+index);
					    				    			   if(ordersPlaced[orders].average_price != null){

					    				    			 		 ordersPlaced.splice(index,1);
					    				    			  }
		    				      		

		    				      		}else {	
				    				    	 	//console.log("--------------START OF LOOP WHEN ORDER:NAME != POSITION:NAME______________________ORDER____>" +ordersPlaced[orders].name+" ---POSITION-- "+obj[position].strategy);
				    				    		//Look in dataBeforeCloseOrder data and In case it has an entry then the previous order for that position existed and now it is closed
				    				    	    //So take the raw p/L for that order name and calculate the total P/L
				    				    	    dataBeforeCloseOrder.push({strategyName:ordersPlaced[orders].name, orders:ordersPlaced[orders]})
				    				    	    if(ordersPlaced[orders].type == "CLOSE"){
				    				    	  	//In case of orders being closed, check if the position was opened today and not the previous day
				    				    	  	//In case of previous day it is normal close and if not it is multitrade scenario
				    				    		    if(!isStrategyClosed(ordersPlaced,ordersPlaced[orders].size,ordersPlaced[orders].name,ordersPlaced[orders].type,ordersPlaced[orders].completed_at)) {
				    				    			    	var closePrice = getClosePrice(ordersPlaced[orders].strategy_id);				    				    		
					    				    			  //  console.log("NOT A MUTITRADE CLOSE SCENARIO BUT  A NORMAL CLOSE ----------------- "+ordersPlaced[orders].name);
					    				    			    //Long Position closed
					    				    			    if(ordersPlaced[orders].size <  0 ){
				 											 if(ordersPlaced[orders].average_price != null){
				 											 	
				 												 gain_loss= ((ordersPlaced[orders].average_price/closePrice));
				 											//	 console.log("NOT A MUTITRADE CLOSE SCENARIO BUT  A NORMAL CLOSE ------Long Position closed----------- "+ordersPlaced[orders].name+" ----- "+ gain_loss);
				 											 }else {
				 												    gain_loss= ((currentPrice/closePrice));
				 												  //   console.log("When the average price is not yet generated in the order_history table_____Long Position closed_______>" + gain_loss);
				 											 }
				 											    										 
				 										    updateData($scope.dailyPandLDAX,ordersPlaced[orders].name,Math.round((gain_loss-1)*10000)/100+'%' )
				 										    // For a closed position which was open overnight, check if another position is going to be opeded. This is to account for the P/L
				 										    //Store this as the RAW P/L
																	if(isStrategyOpen(ordersPlaced,ordersPlaced[orders].size,ordersPlaced[orders].name,ordersPlaced[orders].type,ordersPlaced[orders].completed_at)) {

																			 $scope.strategyName.push({strategyNewName:  ordersPlaced[orders].name, rawPL:parseFloat(gain_loss), positiondata: null,gain_loss_today: null,gain_loss: null});
																			
																			// console.log(" strategyName.push&&&&&&&&&&&&&&&&&&&&&&&&&&&&----------prevPositionPL------9----> "+ obj[position].size);
																			//  console.log("isStrategyOpen __________FUTURE Long Position WILL BE OPENED _________________>" +ordersPlaced[orders].name+" ----- "+ gain_loss);    
				 											 		}else{
 																			if(((ordersPlaced[orders].name.indexOf('Catana DAX')!== -1  || ordersPlaced[orders].name.indexOf('Catana DM')!== -1) && !(isNaN(gain_loss)))){
										  	  			   	          			totalPandL+= (Math.round((gain_loss-1)*10000)/100)* ((ordersPlaced[orders].average_price*25*Math.abs(ordersPlaced[orders]))/totalNAV);
										  	  			   	          			totalPandLNew+= (Math.abs(ordersPlaced[orders].size)*closePrice*1*(Math.round((gain_loss-1)*10000)/100));
																				
										  	  			   	          			console.log("totalPandLNew__________VERY IMPORTANT________________>" +obj[position].strategy+" ----- PandL-->"+ ((Math.round((gain_loss-1)*10000)/100)) +" ------------------------closePrice-->"+closePrice);

										  	  			   	          	}
				 											 		}
				 											 		//console.log("ENd of the closed loop __________Long Position closed_________________>" +ordersPlaced[orders].name+" ----- "+ gain_loss); 
				 										    //Short Position closed
				 										 }else if(ordersPlaced[orders].size > 0) {
															 if(ordersPlaced[orders].average_price != null){
																 //Check if the Short  position  was opened on the same day
																 	   	 gain_loss= ((closePrice/ordersPlaced[orders].average_price))
																 	 //  	 console.log("NOT A MUTITRADE CLOSE SCENARIO BUT  A NORMAL CLOSE ------Short Position closed----------- "+ordersPlaced[orders].name+" ----- "+ gain_loss);
																}else{
																		 gain_loss= ((closePrice/currentPrice))
																		//  console.log("When the average price is not yet generated in the order_history table_____Short Position closed_______>" + gain_loss);
																}
																			    												    									
															
															 updateData($scope.dailyPandLDAX,ordersPlaced[orders].name,Math.round((gain_loss-1)*10000)/100+'%' )
															 if(isStrategyOpen(ordersPlaced,ordersPlaced[orders].size,ordersPlaced[orders].name,ordersPlaced[orders].type,ordersPlaced[orders].completed_at)) {

																			 $scope.strategyName.push({strategyNewName: ordersPlaced[orders].name, rawPL:parseFloat(gain_loss), positiondata: null,gain_loss_today: null,gain_loss: null});
																			
																			//  console.log("isStrategyOpen __________FUTURE SHORT Position WILL BE OPENED _________________>" +ordersPlaced[orders].name+" ----- "+ gain_loss); 
																			//  console.log(" strategyName.push&&&&&&&&&&&&&&&&&&&&&&&&&&&&----------prevPositionPL------10----> "+ obj[position].size);   

				 											 }else{

							 											 if(((ordersPlaced[orders].name.indexOf('Catana DAX')!== -1  || ordersPlaced[orders].name.indexOf('Catana DM')!== -1) && !(isNaN(gain_loss)))){
										  	  			   	          			
										  	  			   	          			totalPandLNew+= (Math.abs(ordersPlaced[orders].size)*closePrice*1*(Math.round((gain_loss-1)*10000)/100));
																					
										  	  			   	          		//	console.log("totalPandLNew__________VERY IMPORTANT________________>" +obj[position].strategy+" ----- PandL-->"+ ((Math.round((gain_loss-1)*10000)/100)) +" ------------------------closePrice-->"+closePrice);

										  	  			   	          	}
				 											 	}
				 											//  console.log("ENd of the closed loop __________Short Position closed_________________>" +ordersPlaced[orders].name+" ----- "+ gain_loss);   
							 
														 }   	
					    				    			  
					    				    			 
					    				    			  var index = ordersPlaced.indexOf(ordersPlaced[orders]);
					    				    			  //console.log("index -------------------------index "+index);
					    				    			   if(ordersPlaced[orders].average_price != null){

					    				    			 		 ordersPlaced.splice(index,1);
					    				    			  }
					    				    			  //TO-DO: Check if there was a multitrade and the position was then closed.
					    				    		  }//ENd of isStrategyClosed
					    				    		  else{
													
													
													
													
					    				    		  }	
					    				    		  
					  	    				    	
				    				    	  		}//if(ordersPlaced[orders].type == "CLOSE"){
		    				    	  		}//end of ELSE ____
		    				    	  
		    				      }//end of CLOSE NOT IN POSITION 
		    				}
		    				//console.log("positionNotInOrder ---------END OF LOOPING ORDER TABLE---------------- IMPORTANT_________________>" +positionNotInOrder);
		    				if(!positionNotInOrder){
		    					 if((   (obj[position].strategy.indexOf('Catana DAX')!== -1 || obj[position].strategy.indexOf('Catana DM')!== -1  )  ) &&(isNaN(gain_loss)) ){

				   	          		//console.log("AM HERE 1");
				   	          		totalPandL+= (Math.round((gain_loss-1)*10000)/100)* ((currentPrice*1*Math.abs(obj[position].size))/totalNAV);
				   	          	//	console.log("totalPandLNew__________VERY IMPORTANT_______1_________>" +obj[position].strategy+" ----- "+ ((Math.round((gain_loss-1)*10000)/100)) +" -----------------closePrice---------"+obj[position].open_price);
				   	          		//if(ordersPlaced[orders].type == "CLOSE")
				   	          		   totalPandLNew+= (Math.abs(obj[position].size)*closePrice*1*(Math.round((gain_loss-1)*10000)/100))
				   	          		  
				   	          	  
				   	          	}			
								console.log("END OF LOOP -CALCULATING P/L-CASH FLOW  ___________VERY IMPORTANT_______________>" +obj[position].strategy+" ----- "+ (totalPandLNew));
							
		    				}
		    				if(positionNotInOrder){
					    				//NORMAL P/L calculation		    	  
					    			  //  console.log("IN positionNotInOrder TABLE BUT EXISTS IN POSITION----" +obj[position].strategy);
					    			    // For calculating the individual P/L when there are no orders in order table but there is a position existing
					             		//Firt check if is  a short or long
					   	          		var closePrice = getClosePrice(obj[position].strategy_id);
					   	          		 if(obj[position].size < 0 ){	          				 
					   	          				
					   	          			 gain_loss =((closePrice/currentPrice)-1);	
					   	          			 updateData($scope.dailyPandLDAX,obj[position].strategy,Math.round(gain_loss*10000)/100+'%' );
					   	          			 $scope.strategyName.push({strategyNewName: replaceStrategyName(obj[position].strategy), rawPL:null,positiondata: obj[position],currentPrice: currentPrice, next_stop:(Math.round(((currentPrice -obj[position].next_stop)/currentPrice)*10000)/100) ,updated_at :updated_at,gain_loss_today:Math.round(gain_loss*10000)/100+'%' , gain_loss: Math.round((obj[position].gain_loss)*10000)/100+'%' });
					   	          			 //console.log(" strategyName.push&&&&&&&&&&&&&&&&&&&&&&&&&&&&----------prevPositionPL------11----> "+ obj[position].size);
					   								
					   	          		  }else if(obj[position].size > 0) {
					   	          				 
					   	          			 gain_loss = ((currentPrice/closePrice)-1);
					   	          			//console.log(" strategyName.push&&&&&&&  "+(Math.round(((currentPrice -obj[position].next_stop)/currentPrice)*10000)/100)+"  &&&&&&&&&&&&&&&&&&&&& "+currentPrice);
					   	          			 updateData($scope.dailyPandLDAX,obj[position].strategy,Math.round(gain_loss*10000)/100+'%' );
					   	          			 $scope.strategyName.push({strategyNewName: replaceStrategyName(obj[position].strategy), rawPL:null,positiondata: obj[position],currentPrice: currentPrice,next_stop:(Math.round(((currentPrice -obj[position].next_stop)/currentPrice)*10000)/100) , updated_at :updated_at,gain_loss_today:Math.round(gain_loss*10000)/100+'%'  ,gain_loss: Math.round((obj[position].gain_loss)*10000)/100+'%' }); 
					   	          			//console.log(" strategyName.push&&&&&&&&&&&&&&&&&&&&&&&&&&&&----------prevPositionPL------12----> "+ obj[position].size);
					   	          		 }
					   	          		for (var i = 0; i < $scope.strategyName.length; i++) {
					   	  							//console.log("ININ positionNotInOrder!!!!!LOOP SRATEGYNAME ************* "+$scope.strategyName[i].rawPL+" *************************"+$scope.strategyName[i].strategyNewName+"**********************"+obj[position]);
					     				}
						   	          
						   	          	if((obj[position].strategy.indexOf('Catana DAX')!== -1 || obj[position].strategy.indexOf('Catana DM')!== -1  ) && !(isNaN(gain_loss))){
						   	          		
						   	          		//console.log("totalPandLNew__________VERY IMPORTANT_____2____________>" +obj[position].strategy+" ----- "+ ((Math.round((gain_loss)*10000)/100)) +" --------------------closePrice------"+closePrice);
						   	          		totalPandLNew+= (Math.abs(obj[position].size)*closePrice*1*(Math.round((gain_loss)*10000)/100))
						   	          		
						   	          		 

						   	          	}  
						   	          		
						   	          	//	console.log(" END OF LOOP IN positionNotInOrder TABLE BUT EXISTS IN POSITION___________2_________________>" +obj[position].strategy+" ----- "+ (totalPandLNew));
		    				}  			   	
		    				if((obj[position].strategy.indexOf('Catana DAX')!== -1 || obj[position].strategy.indexOf('Catana DM')!== -1  )){
		    					
		    				    	// For calculating the Exposure for the DAX future
								   var test = obj[position].size ;
			          			   var test2 = (currentPrice);
			          			   // size(s) x current price(s) x factors for future(s) x currency(s)
			          			   exp+= test * test2 * 1 * 1;
			          			  // console.log("OPENNNN  exp DAX "+exp);
		    				}	
		    								  
		    			
		          			
		    		  }else if(obj[position].strategy_id == 19){
		    		  	//console.log("Catana Retail Strategy----------------------->>>>>  ");
		    			  $scope.stocksRetailPositions = true;
		    			//  $scope.strategyName.push(obj[position]);
		    			// To get the previous day close price, check at 18:30 Everyday except Saturday and sunday. 
		    				//At this time the exchange rate is recorded so that we dont have to make the call everyfew second.
		    				//check if the current time is 18:30 	    				
		    			   	if(  ((new Date().getHours() + ":" + new Date().getMinutes()) == "18:30") || !yesterdayClosePriceExists){
		    				
		    			   		calculateYesterdayClosePrice( obj[position].title_id,obj[position].strategy_id,$scope, $http);
		    			       				
		    				}	
		    			  //To check for the short position
		    			  if(obj[position].size < 0 ){
		              			var test = (obj[position].size) ;
		              			var test1 = ((obj[position].open_price));
		              			var test2 = ((obj[position].current_price));
		              			StocksRetailopen1+=  test1 * test;
		              			StocksRetailcurrent1+= test2 * test;	                			 
		    			  }else{
		    				  var test = (obj[position].size) ;
		              			var test1 = ((obj[position].open_price));
		              			var test2 = ((obj[position].current_price));
		              			StocksRetailopen2+= test1 * test;
		              			StocksRetailcurrent2+= test2 * test;
	          			 
		    			  }
		    			
		    			 // console.log("OPENNNN  "+StocksLongopen1);
		    			 
		    		  }


		    	   }

		    	
	});
		 
	    	  
	             //FOR calculating the summary of RETAIL stocks    	  
	          StocksRetailopen1= StocksRetailopen1*100;
	       //   console.log("StocksLongopen1.*****  "+ StocksLongopen1 );

	          StocksRetailcurrent1 = StocksRetailcurrent1*100;

	          
	         // Alternative way of calculating the p/L for individual positions for RETAIL stocks
	          var valStocksRetail1 = (StocksRetailcurrent2 - StocksRetailopen2 ) + (Math.abs(StocksRetailopen1) - Math.abs(StocksRetailcurrent1));	
	                //  console.log("valStocksLong1.*****  "+ valStocksLong1 );        
	          var valueStocksRetail = valStocksRetail1/(Math.abs(StocksRetailcurrent1) + StocksRetailcurrent2);
	        
	          $scope.stocksRetailGainLoss = (Math.round(valueStocksRetail*10000)/100)+'%';
	          $scope.stocksRetailOpenPrice = Math.abs(StocksRetailopen1)+ StocksRetailopen2;
	          $scope.stocksRetailCurrentPrice= Math.abs(StocksRetailcurrent1)+ StocksRetailcurrent2;

	       //   console.log("StocksRetailcurrent1.*****  "+ StocksRetailcurrent1 );
	          //For calculating the Exposure for the Long stocks
	            exp+= StocksRetailcurrent1 * 1  + StocksRetailcurrent2 *1;	
	            //$scope.exposureVal=((Math.round((exp/ totalNAV)*10000)/100)+'%');
	      	                  
	         exchangeClosedRateExists = true;
	         
	          
	          
	          angular.forEach(positionsData, function(obj, key) {
		    	//  console.log(key + ': ' + obj);			
					
		    	  for (var position in obj) {
		    		 

			    	    if(obj[position].strategy_id === 19){
																//	console.log("IN RETAILLLLLL!!!!!!!!!!!!!!! " +closePrice);
			    	   	// When there is a position, check if there is any order history for that strategy for that time	    			   	
		    			   	var positionNotInOrder = true;
		    			  
		    			// For calculating the individual P/L when there are no orders in order table but there is a position existing
		    			   //First check if is  a short or long
		         			 var closePrice = getClosePrice(obj[position].isin);
		         			 for (var orders in ordersPlaced  )    {	
			    					
			    					if(ordersPlaced[orders].name != "IB TEST Retail Strategy" && ordersPlaced[orders].confirmed_at != null && obj[position].title_id == ordersPlaced[orders].title_id)  {
			    					console.log("IN RETAILLLLLL!TYPE   !!!!!!!!!!!!!! " +ordersPlaced[orders].strategy_id)
			    						positionNotInOrder= false;
			    						 var gain_loss_today = 0;
			    						//get the Current price in the particular order(Execution price)
			    						var currentPrice = ordersPlaced[orders].average_price;
			    						//get the order type. If it is close then P/L is  current price in Orderhistory/yesterday close at 17:30
			    						 var closePrice = getClosePrice(obj[position].isin);
			    								   
			    								
			    								if(ordersPlaced[orders].type == "CLOSE"){
			    									 if(obj[position].size > 0 ){	
			    										 if(ordersPlaced[orders].average_price != null){
			    											 gain_loss_today =ordersPlaced[orders].average_price/closePrice; 
			    											 dailyValStocksRetail+=((Math.abs(obj[position].size)*obj[position].current_price)/$scope.stocksRetailCurrentPrice)*((ordersPlaced[orders].average_price/closePrice)-1);

			    										 }else {
			    											 gain_loss_today = obj[position].current_price/closePrice;
			    											 dailyValStocksRetail+=((Math.abs(obj[position].size)*obj[position].current_price)/$scope.stocksRetailCurrentPrice)*((obj[position].current_price/closePrice)-1);
			    										 }
			    										//TO-DO: Multitrading scenario for LONG and Nuetral STOCKS:		    									
				    								/*	 var prevPositionPL = CheckIfStocksPositionPresent($scope.dailyPandLTable,obj[position].en);
				    									 if(prevPositionPL != null){
				    										 gain_loss_today = (prevPositionPL * gain_loss_today );
				    										 $scope.dailyPandLTable.push({ strategy: obj[position].strategy, rawPL:parseFloat(gain_loss_today-1),en: obj[position].en, isin: obj[position].isin, position: obj[position].position, size:  obj[position].size,  open_price: obj[position].open_price, current_price: obj[position].current_price, gain_loss: (Math.round(obj[position].gain_loss*10000)/100)+'%', gain_loss_today:  Math.round(parseFloat(gain_loss_today -1)*10000)/100+'%', updated_at: obj[position].updated_at });
				    									 }else {*/
				    										 $scope.dailyPandLTableRetail.push({ strategy: obj[position].strategy, rawPL:parseFloat(gain_loss_today-1),en: obj[position].en, isin: obj[position].isin, position: obj[position].position, size:  obj[position].size,  open_price: obj[position].open_price, current_price: obj[position].current_price,next_stop:(Math.round(((obj[position].current_price -obj[position].next_stop)/obj[position].current_price)*10000)/100) , gain_loss: (Math.round(obj[position].gain_loss*10000)/100)+'%', gain_loss_today:  Math.round(parseFloat(gain_loss_today -1)*10000)/100+'%', updated_at: obj[position].updated_at });
				    										
				    										
				    							 }
			    									 
			    								}else{
			    									 if(obj[position].size > 0 ){
			    										 if(ordersPlaced[orders].average_price != null){
			    											 gain_loss_today =  obj[position].current_price/ordersPlaced[orders].average_price;
			    											 dailyValStocksRetail+=((Math.abs(obj[position].size)*obj[position].current_price)/$scope.stocksRetailCurrentPrice)*((obj[position].current_price/ordersPlaced[orders].average_price)-1);
			    										 }else {
			    											 gain_loss_today =  obj[position].current_price/obj[position].current_price;
			    											 dailyValStocksRetail+=((Math.abs(obj[position].size)*obj[position].current_price)/$scope.stocksRetailCurrentPrice)*((obj[position].current_price/obj[position].current_price)-1);
			    										 }
			    										//TO-DO: Multitrading scenario for LONG and Nuetral STOCKS:
			    										/* var prevPositionPL = CheckIfStocksPositionPresent($scope.dailyPandLTable,obj[position].en);
				    									 if(prevPositionPL != null){
				    										 gain_loss_today = (prevPositionPL * gain_loss_today );
				    										 $scope.dailyPandLTable.push({ strategy: obj[position].strategy, rawPL:parseFloat(gain_loss_today-1),en: obj[position].en, isin: obj[position].isin, position: obj[position].position, size:  obj[position].size,  open_price: obj[position].open_price, current_price: obj[position].current_price, gain_loss: (Math.round(obj[position].gain_loss*10000)/100)+'%', gain_loss_today:  Math.round(parseFloat(gain_loss_today -1)*10000)/100+'%', updated_at: obj[position].updated_at });
				    									 }else {*/
				    										$scope.dailyPandLTableRetail.push({ strategy: obj[position].strategy, rawPL:parseFloat(gain_loss_today-1),en: obj[position].en, isin: obj[position].isin, position: obj[position].position, size:  obj[position].size,  open_price: obj[position].open_price, current_price: obj[position].current_price, next_stop:(Math.round(((obj[position].current_price -obj[position].next_stop)/obj[position].current_price)*10000)/100) ,gain_loss: (Math.round(obj[position].gain_loss*10000)/100)+'%', gain_loss_today:  Math.round(parseFloat(gain_loss_today -1)*10000)/100+'%', updated_at: obj[position].updated_at });
				    										// console.log("totalPandLNew__________VERY IMPORTANT_____2____________>" +obj[position].en+" ----- "+ ((Math.round(parseFloat(gain_loss_today -1)*10000)/100)) +" ------------------------open_price--"+obj[position].open_price);
				    										
				    										
			    												    										 
			    									 }	    							
			    						        }
			    								
			    								//console.log("NEWPL FOR LONG  ____________________________>" +obj[position].strategy+" ----- "+ ($scope.dailyPandLTable ));
			    					
			    				    }else {	    				    	  
			    				    	  //NORMAL P/L calculation
			    				    	  
			    				    	  
			    				      }
			    		       }
			    			   	if(positionNotInOrder){

			    			   	var currentPrice = getCurrentPrice(obj[position].title_id);
		    						if(currentPrice == null)
		    							 currentPrice = obj[position].current_price 

		    						//	console.log("currentPriceTH!!!!! " +currentPrice);

		    							var updated_at = getUpdatedDate(obj[position].title_id);
		    							// console.log("IN updated_at---------------->"+updated_at);
									if(updated_at == null)
		    							 updated_at = obj[position].updated_at

			    			   			if(!(isNaN(closePrice)))
				         				 dailyValStocksRetail+=((Math.abs(obj[position].size)*currentPrice)/$scope.stocksRetailCurrentPrice)*((currentPrice/closePrice)-1);
				         				 if(!(isNaN(closePrice)))
				         				 var PLToday = Math.round(((currentPrice/closePrice)-1)*10000)/100+'%';
				         				 else
				         				 	var PLToday = "N/A"
										 $scope.dailyPandLTableRetail.push({ strategy: obj[position].strategy, en: obj[position].en, isin: obj[position].isin, position: obj[position].position, size:  obj[position].size,  open_price: obj[position].open_price, current_price: currentPrice, next_stop:(Math.round(((currentPrice -obj[position].next_stop)/currentPrice)*10000)/100) ,gain_loss: (Math.round(obj[position].gain_loss*10000)/100)+'%', gain_loss_today: PLToday, updated_at: updated_at });
										
										

									
			    			   	}

			    			  // console.log("IN RETAILLLLLL!!!!!!!!!!LENGTH!!!!! " +$scope.dailyPandLTableRetail.length);
			    	   }			
		    	  }
		    	  	
	          });
			         
	          	// For calculating the P/L Daily for Retail Stock
		        $scope.PandLRetailStok= Math.round((dailyValStocksRetail)*10000)/100;
		        //   console.log("IN dailyValStocksLong!!!!!!!!!!!!!!!FINAL 3  " + (stocksNuetralCurrentSummary/totalNAV)) ;
	         // 	console.log("IN dailyValStocksLong!!!!!!!!!!!!!!!FINAL 3 totalpl " + (totalPandL)) ;
	         
	          	//For calculating the summary TOTAL P&L
	          	if(!(isNaN(dailyValStocksRetail)) && !(isNaN(totalNAV)))
	        	          totalPandLNew+= $scope.PandLRetailStok * (dailyValStocksRetail/totalNAV);;
	        	      	totalCurrent+=$scope.stocksRetailCurrentPrice
				//console.log("IN dailyValStocksLong!!!!!!!!!!totalPandLNew" + (totalPandLNew)) ;
			     yesterdayClosePriceExists = true;
			     oderHistoryTableChanged = true;
		    	// This part is to clean out any strategies wwith positions which have been added only to account of rawPL
		     	 $scope.tempList = [];		
					 for (var position in $scope.strategyName) {
					 	//console.log("IN okey----------------------------->>>> "+ $scope.strategyName[position].rawPL+" name " + $scope.strategyName[position].strategyNewName);
					     // This is the case where P/L has to be accounted for previously overnight position opened - which is now closed and there is no position in position		  
						 if($scope.strategyName[position].positiondata == null && $scope.strategyName[position].gain_loss == null 	){
						 	//	console.log("$scope.strategyName removing unwanted strategies ------>   " + $scope.strategyName[position].strategyNewName);				 			    
			 					  
						}  	else {

								$scope.tempList.push($scope.strategyName[position]);
			         	} 

			     } 
			     $scope.strategyName = JSON.parse(JSON.stringify($scope.tempList));
		   	      for (var i = 0; i < $scope.strategyName.length; i++) {
			    //	  console.log("IN updateData!!!!!!!!!!!!!!!FINAL****1***1***1*****TEMAAAAAAAAAAAA*****1******1******1******"+$scope.strategyName[i].strategyNewName);
			      }

		      angular.forEach( $scope.strategyName, function(obj, key) {
			
			//console.log("IN okey----------------------------->>>> "+key);
			 for (var position in obj) {	
			     // This is the case where P/L has to be accounted for previously overnight position opened - which is now closed and there is no position in position		  
				 if(obj[position] == null && obj.gain_loss == null 	){
				 	//	console.log("$scope.strategyName removing unwanted strategies ------>   " + obj.strategyNewName);
				 	//	console.log("IN okey----------------------------->>>> "+key);
		 				$scope.strategyName.splice(key,1);
					     break;
	 					  
				}  	
	         } 
	     });

	  



		      /*var currentdate = new Date();
		      var currenttime = currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
					console.log("currenttime***************1******************>>>>"+currenttime);
	     	if( currenttime >= "15:41:20" && currenttime <= "15:41:40" && !plDailyUpdatedInDb ){
	     	  	console.log("currenttime*********************************>>>>"+currenttime);
	     	  		console.log("currenttime*********************************>>>>"+dayName);
	     	  		console.log("currenttime*********************************>>>>"+$scope.totPLCashFlow);
	     	  			var dayName = getDayOfTheWeek();
							var dataObj = {
											day : dayName,
											pl: $scope.totPLCashFlow,
											updated_at: new Date()
											
									};	
									$http({
							  url: 'https://shadownav.catanacapital.de:3000/dailypls',
							  method: "POST",
							  data: $.param(dataObj),
							  headers: {
							    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
							  }
							})
						plDailyUpdatedInDb = true;

				}
				if( currenttime > "15:41:40"){
						plDailyUpdatedInDb = false;
			 
				}	*/

if(!prevPLChanged){
			//Calculation of P/L weekly
			//(p/L(Monday)*P/L(Tuesday)......P/L(Friday)) - 1
			//Read from the table to get each days P/L and apply the formula
			getDailyPL()		      
		    var  day = getDayOfTheWeek(); 
		    var wPL = 1;
		    var dayPresentInDb = false;
		   //console.log(dailyPLDataInDB.length)
		      for (var i = 0; i < dailyPLDataInDB.length; i++) {
		      	//Check if todays PL is already in DB, current days PL is saved at 17:30 everyday, 
		      	//If the weekly PL is calculated after that then consider that PL from DB, else running PL
		      	 if(day == dailyPLDataInDB[i].day){
		      	 		dayPresentInDb = true;    		     

		     	 }
	     	 	 wPL = wPL * (1 + parseFloat(dailyPLDataInDB[i].pl)  ) 
		    	
		        }
		    if(!dayPresentInDb)
		    	wPL = wPL * (parseFloat($scope.totPLCashFlow)+1);
		        $scope.weeklyPL = (wPL-1).toFixed(2)
		     
	      getDailyPLForTotalNav(function(data) { 
	      	                
	                   dailyPrevDayPLDataInDB = data;
	                   // console.log(totalPandLNew)
				
	            for (var i = 0; i < dailyPrevDayPLDataInDB.length; i++) {
	                //Check if todays PL is already in DB, current days PL is saved at 17:30 everyday, 
	                //If the weekly PL is calculated after that then consider that PL from DB, else running PL
	                    $scope.totPLCashFlow = (parseFloat(totalPandLNew/(dailyPrevDayPLDataInDB[i].totalnav)).toFixed(2));
	                 
	                    //console.log(dailyPrevDayPLDataInDB[i].totalnav)
	                    $scope.totalSummaryNav =(parseFloat(dailyPrevDayPLDataInDB[i].totalnav ) *(( $scope.totPLCashFlow/100))) +parseFloat(dailyPrevDayPLDataInDB[i].totalnav)
	                    console.log($scope.totalSummaryNav )
	                    console.log(dailyPrevDayPLDataInDB[i].totalnav )


	               }    
	 		})
  }
  var currentPriceForNav = getCurrentPrice("4000003");
  console.log("IN RETAILLLLLL!!!!!!!!!!!!!!! FINAL" +totalCurrent);
  console.log("IN RETAILLLLLL!!!!!!!!!!!!!!! FINAL" +(currentPriceForNav*3.45)+8300);
  //console.log("IN RETAILLLLLL!!!!!!!!!!!!!!! FINAL" +((currentPriceForNav*3.45)+8300+totalCurrent)); 
  $scope.totalSummaryNav = ((currentPriceForNav*3.45)+8300+totalCurrent)
   $scope.exposureVal=((Math.round((exp/ $scope.totalSummaryNav)*10000)/100)+'%');

	}//End of getPositions     
			
	function updateData(dailyPandLDAX,strategy,gain_loss) {
	    var objects = $scope.dailyPandLDAX;

	    for (var i = 0; i < objects.length; i++) {
	        if (objects[i].strategy == strategy) {
	        	// console.log("IN updateData!!!!!!!!!!!!!!!FINAL   " + objects[i].strategy);
	        
	            objects[i].gain_loss = gain_loss;
	            break;
	        }
	    }
	}
	//CheckIfPositionPresent($scope.strategyName,ordersPlaced[orders].name);rawPL
	function CheckIfPositionPresent(strategyName,orderHistoryStrName,orderType) {
		// console.log("IN CheckIfPositionPresent!!!!!!!!!!!!!!!orderType 1   " + orderType);
		 for (var i = 0; i < $scope.strategyName.length; i++) {
		   	 // console.log("IN CheckIfPositionPresent!!!!!!LOOP SRATEGYNAME ************* "+$scope.strategyName[i].rawPL+" *************************"+$scope.strategyName[i].strategyNewName+"**********************"+$scope.strategyName[i].position);
		     }
		var rawPL = null;
		angular.forEach( $scope.strategyName, function(obj, key) {
			
			//console.log("IN okey----------------------------->>>> "+key);
			 for (var position in obj) {	
			     // This is the case where P/L has to be accounted for previously overnight position opened - which is now closed and there is no position in position		  
				 if(obj[position] == null && obj.rawPL != null 	){
				// 	console.log("IN CheckIfPositionPresent!!!!! orderHistoryStrName !!!!!!!!!!orderHistoryStrName-->   " + orderHistoryStrName);
				 		if ( obj.strategyNewName == orderHistoryStrName) {
		 					rawPL = obj.rawPL;
		 				//	console.log("IN CheckIfPositionPresent!!!!! overnight position opened !!!!!!!!!!rawPL-->   " + rawPL);
		 					//console.log("IN okey----------------------------->>>> "+key);
		 					//if(orderType == "CLOSE")
		 					  $scope.strategyName.splice(key,1);

									 for (var i = 0; i < $scope.strategyName.length; i++) {
										//   	  console.log("IN CheckIfPositionPresent!!!!!!LOOP SRATEGYNAME ************* "+$scope.strategyName[i].rawPL+" *************************"+$scope.strategyName[i].strategyNewName+"**********************"+$scope.strategyName[i].position);
										     }
										     break;
	 					 }  
				}  	else if(obj[position] != null){
				        	 if(obj[position].strategy == orderHistoryStrName) {
				           //arr.splice(arr.pop(item));
				        	// console.log("IN CheckIfPositionPresent!!!!!!!!!!!!!!!obj[position].strategy-->   " + obj[position].strategy);
				        	  rawPL = obj.rawPL;
				        	// console.log("IN CheckIfPositionPresent!!!!!!!!!!!!!!!rawPL-->   " + rawPL);
				        	// if(orderType == "CLOSE")
				        	  $scope.strategyName.splice(key,1);
				        	
	       				 }	
	        		} 
	         } 
	     });
	return rawPL;
	   // submitFields.push({"field":field,"value":value});
	  
	}


	//THIS IS NOT NEEDED; OLD METHOD OF GETTING TICK DATA
	
	function updateTickData(currrentPriceFromTickData,title_id,price,dte) {
	    var objects = currrentPriceFromTickData;
	    var flag = false;
	   //console.log("IN updateTickData!!!!!!" + objects.length);

	    for (var i = 0; i < objects.length; i++) {
	        if (objects[i].title_id == title_id) {
	        	 //console.log("IN updateTickData!!!!!!!!!!!!!!!FINAL   " + objects[i].title_id);
	        //console.log("IN updateTickData!!!!!!!!!!!!!!!COUNTER <<<<< " + objects[i].counter );
	             objects[i].counter =  objects[i].counter + 1
	             if(objects[i].counter == 5){
	             	//console.log("IN updateTickData!!!!!!!!!!!!!!!COUNTER   " + objects[i].counter );
	            			objects[i].currentPrice = price;
	            			objects[i].counter = 0;
	            			objects[i].updated_at = dte;
	            				
	             }
	             flag = true;
	            break;
	        }
	    }
	    if(!flag) {
	    	var count = 0;
	    	  currrentPriceFromTickData.push({title_id: title_id , currentPrice:price, counter: count,updated_at: dte}); 
	        }
	}
/**
	function updateTickData(currrentPriceFromTickData,title_id,price,dte) {
	    var objects = currrentPriceFromTickData;
	    var flag = false;
	   //console.log("IN updateTickData!!!!!!" + objects.length);

	    for (var i = 0; i < objects.length; i++) {
	        if (objects[i].title_id == title_id) {
	        //	 console.log("IN updateTickData!!!!!!!!!!!!!!!FINAL   " + objects[i].title_id);
	            objects[i].currentPrice = price;	            
	         	objects[i].updated_at = dte;   				
	             flag = true;
	            break;
	        }
	    }
	    if(!flag) {
	    	//console.log("IN updateTickData!!  FIRST TIME!!!!");
	    	  currrentPriceFromTickData.push({title_id: title_id , currentPrice:price, updated_at: dte}); 
	        }
	}**/

	function checkIfPositionClosed(strategyName,orderHistoryStrName,orderType) {
		
		angular.forEach(strategyName, function(obj, key) {
			
			  for (var position in obj) {			  
				//  console.log("IN orders in ordersPlaced------------obj[position].strategy-----------------checkIfPositionClosed" + obj[position].strategy);
	      if (obj[position].strategy != orderHistoryStrName) {
	    	 // console.log("IN orders in ordersPlaced is true" + name); 
				return true;
	    	  
	      }  } });
		return false;
	}
	//if(isStrategyClosed(ordersPlaced[orders].size,ordersPlaced[orders].name,ordersPlaced[orders].type)) {
	function isStrategyClosed(ordersPlaced,size,name,orderType,time) {	
		for (var orders in ordersPlaced) {	
		//	console.log("IN orders in ordersPlaced----------------------------->isStrategyClosed");
			
			if(ordersPlaced[orders].name === name && ordersPlaced[orders].confirmed_at != null  )  {
				if((ordersPlaced[orders].size < 0) && (ordersPlaced[orders].type !=orderType && (ordersPlaced[orders].completed_at > time))) {
				//	console.log("IN orders in ordersPlaced is true" + name); 
					return true;
				}
			
				 
			}
		
		}	
		return false;
	  }

	// For a closed position which was open overnight, check if another position is going to be opeded. This is to account for the P/L
	function isStrategyOpen(ordersPlaced,size,name,orderType,time) {	
		for (var orders in ordersPlaced) {	
		//	console.log("IN orders in ordersPlaced-------OPEN---------------------->isStrategyClosed");
			
			if(ordersPlaced[orders].name === name && ordersPlaced[orders].confirmed_at != null )  {
				if((ordersPlaced[orders].size > 0) && (ordersPlaced[orders].type !=orderType && (ordersPlaced[orders].completed_at > time))) {
				//	console.log("IN orders in ordersPlaced is true" + name); 
					return true;
				}			 
			}
		}	
		return false; 
	}


	//TO-DO: Multitrading scenario for LONG and Nuetral STOCKS:
	function CheckIfStocksPositionPresent(dailyPandLTable,orderHistoryStrName) {
		 
			  for (var position in dailyPandLTable) {			  
				  
	       if (dailyPandLTable[position].en == orderHistoryStrName) {
	          //arr.splice(arr.pop(item));
	      // 	 console.log("IN CheckIfPositionPresent!!!!!!!!!!!!!!!FINAL   " +dailyPandLTable[position].en);
	       	 var rawPL = dailyPandLTable[position].rawPL;
	       	// console.log("IN CheckIfPositionPresent!!!!!!!!!!!!!!!FINAL   " + rawPL);
	       	dailyPandLTable.splice(key,1);
	       	return rawPL;

	       }  } 
	return null;
	  // submitFields.push({"field":field,"value":value});
	 
	}

	function replaceStrategyName(strategy) {
		if(strategy == "Catana DAX Future Wed/Thur CONTRA")
			return "DAX Future Tue/Wed(2)";
		else if(strategy == "Catana DAX Wed/Thur")
			return "DAX Tue/Wed";
		else if(strategy == "IB TEST TRADER")
			return "A.I. EUR / USD (test)";
		else
		return strategy.replace("Catana", "");
		
		
	}



	  });


	 app.controller('PopupCont',function ($timeout,$uibModalStack, $interval,$http,$scope,$rootScope, $uibModalInstance,orderList) {
	 	 $scope.IsloggedInPopUp = true;
	 	  $scope.AssignedDate = new Date();

	   var orderIDList = [];
	    var orderIDListRejected = [];
	           // console.log("aseaedqw------------------>"+ $scope.orderList )
	            //on Load of the model first check if the strategy is EURUSD , if it is then automatically confirm it.
	            if(orderList.name == "EURUSD"){

	            	orderIDList.push(orderList.order_id);
	            	//Do a post signal here 
	             	 //https://api2.stockpulse.de/shadow_nav/orders?dev_call=true&format=json
	             	  $http({
	                    url: __env.clientApiUrl+'/shadow_nav/orders?dev_call=true',
	                    data: orderIDList ,
	                    method: "POST",
	                    headers: {
	                        'Content-Type': 'application/json',
	                        'Authorization':'Basic ' + $rootScope.auth
	                    }
	                }).then(function (success){
	                                 console.log(success)
	                                  $scope.IsloggedInPopUp = false;
	                                console.log("Data Posted successfully" );
	                                $scope.message = ("Data Posted successfully" );
	 								$scope.orderList= [];
	 								$interval(function() {
								          $uibModalInstance.close($scope.selected.orderList);
								    }, 5000);
																
	                           },function (error){
	                           	console.log(error)

	                           }); 


	        	}


	             $scope.close = function () {
	                 $uibModalInstance.dismiss('cancel');
	            };

	             $scope.ok = function () {
	             	 console.log("aseaedqw--------okokokokokok---------->"+  $scope.selected.orderList );
	             	 for(var selList in  $scope.selected.orderList){
	             	 	orderIDList.push($scope.selected.orderList[selList].order_id);
	             	 //	console.log("aseaedqw--------okokokokokok---------->"+$scope.selected.orderList[selList].order_id);
	             	 }

	             	 for(var RejList in orderIDListRejected)
	             	 	console.log("aseaedqw--------REJREJREJ---------->"+orderIDListRejected[RejList]);

	             	 
	             	 //Do a post signal here for the rejected orders 
	             	 //https://api2.stockpulse.de/shadow_nav/reject_orders?dev_call=true&format=json
	             	 if(orderIDListRejected.length > 0){
			             	  $http({
			                    url: __env.clientApiUrl+'/shadow_nav/reject_orders?',
			                    data: orderIDListRejected ,
			                    method: "POST",
			                    headers: {
			                        'Content-Type': 'application/json',
			                        'Authorization':'Basic ' + $rootScope.auth
			                    }
			                	}).then(function (success){
			                            console.log(success)	
			                             $scope.IsloggedInPopUp = false;
			                              console.log("Data Posted successfully" );		                                  
			 							orderIDListRejected.length = 0;
			 							//console.log("EMPTY orderIDListRejected" + orderIDListRejected);
			 							if(orderIDList.length <= 0)
			 								 $scope.message = ("Data Posted successfully" );
			 							
				  		 																
			                    },function (error){
			                           	console.log(error)

			                     });  

	 					}	

			 			 if(orderIDList.length > 0){
			             	 //Do a post signal here 
			             	 //https://api2.stockpulse.de/shadow_nav/confirm_orders?dev_call=true&format=json
			             	  $http({
			                    url: __env.clientApiUrl+'/shadow_nav/confirm_orders?',
			                    data: orderIDList ,
			                    method: "POST",
			                    headers: {
			                        'Content-Type': 'application/json',
			                        'Authorization':'Basic ' + $rootScope.auth
			                    }
			                }).then(function (success){
			                                 console.log(success)
			                                  $scope.IsloggedInPopUp = false;		                          
			                                console.log("Data Posted successfully" );
			                                $scope.message = ("Data Posted successfully" );
			 								$scope.orderList.length = 0;
			 								// console.log("EMPTY orderLsit" + $scope.orderList);
	 
			 												
			                           },function (error){
			                           	console.log(error)

			                           });  

				}
	       	  	//NOw close the popup and dismiss all the other popup instances in case or Reject/confirm/both            	  	
	            $scope.message = null;              	
        };


	            var selectRecords = new Array();
				  for(var i=0; i<$scope.orderList.length; i++)
				  {
				   
				      selectRecords.push($scope.orderList[i]);
				  }
	        
	         $scope.checkAll = function() {
	             $scope.selected.orderList = angular.copy($scope.orderList);
	             orderIDListRejected.length= 0;

	          };
	          $scope.uncheckAll = function() {
	              $scope.selected.orderList = [];
	              for(var i=0; i<$scope.orderList.length; i++){
						      orderIDListRejected.push($scope.orderList[i].order_id);
				   }
	             // orderIDListRejected.length = 0;
	          };
	          //Make the List pre-selected with the values
	          $scope.selected = {
						    orderList: selectRecords
						  };

						  $scope.check = function(value, checked) {
						    var idx = $scope.selected.orderList.indexOf(value);
						    if (idx >= 0 && !checked) {
						      orderIDListRejected.push(value.order_id);
						    }
						  
						    if (idx < 0 && checked) {
									      orderIDListRejected.splice(idx, 1);
									    }
									      console.log("rejected...."+orderIDListRejected.length);
						  };
	            $scope.launchModalConditionally = function(){
	                        
	                            $scope.openModal();                       
	            };

	             function getIsin(obj,title_id){

                var isin;
                   var flag = false;
                Object.keys(obj)
          .forEach(function eachKey(key) { 
            if(!flag) {
                  if(obj[key].title_id == title_id){
                    isin = obj[key].isin;
                 flag = true;
               }
                  }
                })
                return isin;

         }

	        });
app.controller('pretradeStockEdit',function (  $window,$http,$scope,$rootScope) {

	 $scope.goHome = function() {             
                console.log("goHome--> " +$rootScope.auth )
                $window.location.href = 'index.html'               
        }
$scope.stocksList= []	        
$http.get('data.json')
	.then(function success(data) {
			console.log("strategy_config");
			console.log(data.data);	

			angular.forEach(data.data, function(obj, key) {
				$scope.stocksList.push(obj)


			 })	
			//$scope.stocksList = data.data		
			
	    })


$scope.signals= [];
// function reloadRoute(){
 
	
 
$scope.tradeTypeList =
    [
        { id: 1, type: "BUY" },
        { id: 2, type: "SELL" },
        { id: 3, type: "FLAT" }
   
    ];
//}

    $scope.rec = {

        'work': {'name':'','strategy_id':'','id':''}
    };


		
    $scope.stocks ={'id':''}
	
        
		 $scope.updateStock = function(selectedOption,item) {

		 	console.log(item)		 	
		 	$scope.signals[selectedOption].title_id= item
		 	
		}
	
		$scope.updateTradeType = function(selOption,tradeTypeVar) {
		 	console.log(tradeTypeVar)
		 	console.log(selOption)		 	
		 	$scope.signals[selOption].position= tradeTypeVar.type
		 	 //$scope.tradeType.selected = tradeTypeVar
 	
			  
		}
           

		 $scope.addRow = function () {
			 	console.log($scope.stocksList)
	          	$scope.stocksList.push({
	                name :"",
	                isin: ""
	                
	           });
      	};

      	$scope.removeRow = function(isin){				
		var index = -1;		
		var comArr = eval( $scope.stocksList );
		console.log("asdasdasd   "+comArr )
		for( var i = 0; i < comArr.length; i++ ) {
			if( comArr[i].isin === isin ) {
				console.log("ISIN---->   "+isin )
				index = i;
				break;
			}
		}
		if( index === -1 ) {
			alert( "Something gone wrong" );
		}
		$scope.stocksList.splice( index, 1 );		
	};



	  
		  $scope.submitForm = function(stocksList,filename) {       
								

 
console.log("asdasdasd SUBMIT FORM "  + $scope.stocksList)
 $http({
    url: 'https://shadownav.catanacapital.de:3002/stocksEdit',
    method: "POST",
    data: $scope.stocksList,
   headers: {
			   'Content-Type': 'application/json'
			       }
})
.then(function (success){
	                       alert('FILE saved successfully.');
	                       // $scope.officialPL = null;
	                      console.log("Data Posted successfully" );

	                },function (error){
	                	console.log(error);

	                 });				

		 };

});

})(window.angular);
